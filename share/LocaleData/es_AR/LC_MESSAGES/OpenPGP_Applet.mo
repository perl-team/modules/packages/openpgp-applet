��    &      L  5   |      P     Q     W     c  a   |  "   �            
             *     8  �   H     �     �     �     �               &  !   @     b     s     �  (   �     �  0   �  c   �  ,   \     �     �     �  3   �  g   �  L   U     �     �     �  �  �  	   z	     �	  !   �	  �   �	  0   H
     y
     
     �
     �
     �
     �
  �   �
     �     �     �     �     �     �       '   &     N     ^     w  3   �     �  6   �  j   �  0   i     �     �     �  @   �  p   3  Z   �  $   �     $     8                             &          $       	                  "                                              %               
                              #   !                    About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: yamila gonzalez <yamilabaddouh@gmail.com>
Language-Team: Spanish (Argentina) (http://www.transifex.com/otf/torproject/language/es_AR/)
Language: es_AR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Acerca de Elija las claves ¿Tiene confianza en esas claves? ¿Tiene suficiente confianza en esta clave para utilizarla igualmente? ¿Tiene suficiente confianza en estas claves para utilizarlas igualmente? Cifrar el portapapeles con _Frase de contraseña Salir Huella digital: Nivel de confianza Completa  Error GnuPG Resultados de GnuPG Ocultar destinatarios Ocultar los ID de usuario de todos los destinatarios de un mensaje cifrado. De otra manera, cualquiera que pueda ver el mensaje cifrado puede ver quienes son los destinatarios. ID de Clave Nivel de confianza Marginal Nombre No hay claves disponibles No hay claves seleccionadas Ninguno (No firmar) Applet de cifrado OpenPGP Otros mensajes proporcionados por GnuPG Salida de GnuPG Elija los destinatarios: Firmar mensaje como: Cifrar y firmar el portapapeles con Clave _Pública Estado El portapapeles no contiene datos de entrada válidos. La siguiente clave no es completamente confiable:  Las siguientes claves no son completamente confiables:  Por lo tanto, la operación no puede realizarse. Nivel de confianza Total Nivel de confianza Desconocido ID de usuario: IDs de usuarios: Estas a punto de abandonar el Applet de OpenPGP, ¿estas seguro? Debe seleccionar una clave privada para firmar el mensaje, alguna clave pública para cifrar el mensaje o ambas. Necesita una clave privada para firmar mensajes, o una clave publica para cifrar mensajes. _Descifrar/Verificar el portapapeles _Administrar Claves Abrir _Editor de Textos 