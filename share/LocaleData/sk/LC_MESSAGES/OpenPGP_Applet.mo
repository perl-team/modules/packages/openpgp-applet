��    $      <  5   \      0     1     7     C  a   \  "   �     �     �  
   �     �     
       �   (     �     �     �     �     �     �       !         B     S     f  (   w     �  0   �  c   �  ,   <     i     x     �  g   �  L        N     h  �  u     	     $	     8	  �   V	  ,   2
  	   _
  
   i
     t
     �
     �
     �
  �   �
     P     \     n  #   s      �     �     �  #   �               4  8   L     �  ,   �  �   �  &   d     �     �  3   �  �   �  _   h     �     �                                        #       	                  !                                              $               
                              "                        About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2015-01-30 01:51+0000
Last-Translator: FooBar <thewired@riseup.net>
Language-Team: Slovak (http://www.transifex.com/projects/p/torproject/language/sk/)
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 O aplikácií Zvoľte si kľúče Dôverujete týmto kľúčom? Napriek tomu dôverujete tomuto kľúču tak, že ho hodláte použiť? Napriek tomu dôverujete týmto kľúčom tak, že ich hodláte použiť? Napriek tomu dôverujete týmto kľúčom tak, že ich hodláte použiť? Šifrovať schránku s _prístupovým heslom Ukončiť Odtlačok: Plná dôvera GnuPG chyba GnuPG výsledky Skryť príjemcov Skryť uživateľské ID všetkých príjemcov šifrovanej správy. V opačnom prípade každý, kto túto správu zazrie, bude schopný odhaliť ich identitu. ID kľúča Okrajová dôvera Meno Nie sú dostupné žiadne kľúče. Neboli zvolené žiadne kľúče Žiadne (nepodpisovať) OpenPGP šifrovací aplet Ostatné správy poskytnuté GnuPG: Výstup GnuPG: Zvoľte si príjemcov: Podpísať správu ako: Prihlásiť/Šifrovať schránku s verejnými _kľúčmi Stav Schránka neobsahuje platné vstupné dáta. Nasledujúci zvolený kľúč nie je plne dôveryhodný: Nasledujúce zvolené kľúče nie sú plne dôveryhodne: Nasledujúce zvolené kľúče nie sú plne dôveryhodne: Operácia teda nemohla byť vykonaná. Maximálna dôvera Neznáma dôvera ID užívateľa ID užívateľov: ID užívateľov: Na podpísanie správy si musíte zvoliť súkromný kľúč alebo nejaké verejné kľúče pre jej zašifrovanie, prípadne oboje. Potrebujete súkromný kľúč na podpísanie správ alebo verejný kľúč na ich šifrovanie. _Dešifrovať/Overiť schránku _manažovať kľúče 