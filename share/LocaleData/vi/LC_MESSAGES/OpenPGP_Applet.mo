��    &      L  5   |      P     Q     W     c  a   |  "   �            
             *     8  �   H     �     �     �     �               &  !   @     b     s     �  (   �     �  0   �  c   �  ,   \     �     �     �  3   �  g   �  L   U     �     �     �  �  �     d	     i	  '   ~	  _   �	  $   
     +
     2
     B
     ^
     j
     �
  u   �
          &     A     F  +   b     �     �  <   �       %        B  )   _     �  ;   �  [   �  7   /     g  /   �  !   �  ?   �  �     w   �     !     ;     H                             &          $       	                  "                                              %               
                              #   !                    About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Khanh Nguyen <nguyenduykhanh85@gmail.com>
Language-Team: Vietnamese (http://www.transifex.com/otf/torproject/language/vi/)
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 Về Chọn những khóa Bạn có tin cậy những khóa này? Bạn có tin cậy những khóa này đủ để sử dụng chúng dù thế nào đi nữa? Mã hóa Clipboard với _Passphrase Thoát Dấu vân tay: Sự tin cậy đầy đủ Lỗi GnuPG Những kết quả GnuPG Ẩn những người nhận Ẩn những ID của người dùng của tất cả những người nhận một thông điệp được mã hóa. ID của khóa Sự tin cậy hạn chế Tên Không có sẵn khóa nào Không có khóa nào được lựa chọn Không có gì (Đừng ký) Ứng dụng mã hóa OpenPGP Những thông điệp khác được cung cấp bởi GnuPG Đầu ra của GnuPG Lựa chọn những người nhận: Ký thông điệp như là: Ký/Mã hóa clipboard với Public _Keys Tình trạng Clipboard không chứa dữ liệu nhập vào hợp lệ. Những khóa được lữa chọn dưới đây không được tin cậy tuyệt đối: Cho nên việc vận hành không thể thực hiện. Sự tin cậy tối đa Sự tin tưởng không được biết đến IDs của những người dùng: Bạn định thoát khỏi OpenPGP Applet. Bạn chắc chứ? Bạn phải lựa chọn khóa cá nhân để ký thông điệp, hoặc một vài khóa công khai để mã hóa thông điệp, hoặc cả hai. Bạn cần một khóa cá nhân để ký thông điệp hoặc một khóa công khai để mã hóa thông điệp. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor 