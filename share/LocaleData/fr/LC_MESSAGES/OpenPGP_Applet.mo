��    &      L  5   |      P     Q     W     c  a   |  "   �            
             *     8  �   H     �     �     �     �               &  !   @     b     s     �  (   �     �  0   �  c   �  ,   \     �     �     �  3   �  g   �  L   U     �     �     �  �  �  	   j	     t	  $   �	  �   �	  3   <
     p
  
   x
     �
     �
     �
     �
  �   �
     �     �     �     �     �     �     �  #        <     W     s  8   �     �  5   �  c     /   l     �     �  >   �  6      r   7  f   �  '        9     K                             &          $       	                  "                                              %               
                              #   !                    About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2016-03-21 16:27+0000
Last-Translator: Gwennole Hangard <gwennole.hangard@gmail.com>
Language-Team: French (http://www.transifex.com/otf/torproject/language/fr/)
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 À propos Choisir les clés Faites-vous confiance à ces clés ? Faites-vous quand même assez confiance à cette clé pour l'utiliser ? Faites-vous quand même assez confiance à ces clés pour les utiliser ? Chiffrer le presse-papier avec une _Phrase de passe Quitter Empreinte: Confiance complète Erreur de GnuPG Résultats de GnuPG Cacher les destinataires Cacher les identifiants utilisateur de tous les destinataires d'un message chiffré. Sans quoi n'importe qui interceptant le message chiffré peut savoir à qui il est destiné. Identifiant de la clé Confiance minime Nom Pas de clé disponible Aucune clé sélectionnée Aucun (ne pas signer) Applet de chiffrement OpenPGP Autres messages fournis par GnuPG : Voici la sortie de GnuPG : Choisir les destinataires : Signer le message en tant que : Signer/Chiffrer le presse-papier avec une _clé publique État Le presse-papier ne contient pas de données valides. La clé suivante n'est pas totalement fiable : Les clés suivantes ne sont pas totalement fiables : L'opération ne peut donc pas être effectuée. Confiance absolue Confiance inconnue Identifiant de l'utilisateur : Identifiants de l'utilisateur : Vous allez sortir de l'Applet OpenPGP. Est-vous sûr ? Vous devez sélectionner une clé privée pour signer le message, une clé publique pour le chiffrer, ou les deux. Vous avez besoin d'une clé privée pour signer les messages ou d'une clé publique pour les chiffrer. _Déchiffrer/Vérifier le presse-papier _Gérer les Clés _Ouvrir l'éditeur de texte 