��    &      L  5   |      P     Q     W     c  a   |  "   �            
             *     8  �   H     �     �     �     �               &  !   @     b     s     �  (   �     �  0   �  c   �  ,   \     �     �     �  3   �  g   �  L   U     �     �     �  �  �     j	     q	     ~	  3   �	  +   �	     �	  	   
     
     
     %
     2
  �   B
  	   �
     �
     �
     �
     �
             "   8     [     n       *   �     �  *   �  -   �          6     C     S  8   d  �   �  c   +     �     �     �                             &          $       	                  "                                              %               
                              #   !                    About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Agustín Wu <losangwuyts@gmail.com>
Language-Team: Chinese (Taiwan) (http://www.transifex.com/otf/torproject/language/zh_TW/)
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 關於 選擇金鑰 您信任這些金鑰嗎？ 您真的信任這些金鑰並繼續使用它們？ 使用 _Passphrase 對剪貼簿進行加密 離開 指紋： 完全信任 GnuPG 錯誤 GnuPG 結果 隱藏收件者 將加密郵件中所有收件者的使用者識別碼隱藏，否則任何看到加密郵件的人都可看到此郵件之收件者。 金鑰 ID 邊緣化信任 名稱 無可用的金鑰 沒有選擇金鑰 無(不簽署) OpenPGP 加密小程式 由 GnuPG 提供的其他訊息： GnuPG 的輸出： 選擇收件者: 訊息簽署： 使用公開金鑰簽署或加密剪貼簿 狀態 剪貼簿不包含有效的輸入資料。 以下所選擇的金鑰無法完全信任： 因此不能執行操作。 終極信任 不明的信任 使用者 IDs： 你正準備離開 OpenPGP 加密程式。你確定嗎 ? 您必須選擇一把私密金鑰來對訊息進行簽署，抑或是用幾把公開金鑰來將訊息加密，或兩個方式同時使用。 您需要一把私密金鑰來對訊息進行簽署，或者一把公開金鑰來將訊息加密。 _Decrypt/驗證剪貼簿 _Manage 金鑰 _Open 文字編輯器 