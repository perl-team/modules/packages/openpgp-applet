��    &      L  5   |      P     Q     W     c  a   |  "   �            
             *     8  �   H     �     �     �     �               &  !   @     b     s     �  (   �     �  0   �  c   �  ,   \     �     �     �  3   �  g   �  L   U     �     �     �  �  �     a	     g	     ~	  �   �	  .   *
     Y
     _
     h
     ~
     �
     �
  �   �
     e     n     �     �  %   �     �     �  '   	     1     C     [  C   w     �  C   �  �     /   �     �     �  '   �  2   #  �   V  k   �  ,   Q     ~     �                             &          $       	                  "                                              %               
                              #   !                    About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Besnik <besnik@programeshqip.org>
Language-Team: Albanian (http://www.transifex.com/otf/torproject/language/sq/)
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Rreth Përzgjidhni çelësat A i besoni këta çelësa? A e beson mjaftueshëm këtë çelës, sa ta përdorësh atë gjithsesi? A i besoni mjaftueshëm këta çelësa, sa t'i përdorni ata gjithsesi? Fshiheni Tabelën e Shënimeve me _frazëkalim Dilni Gjurmë: Besueshmëri e Plotë Gabim i GnuPG Rezultatet GnuPG Fshihni marrësit Fshihni ID-të e përdoruesve të të gjithë marrësve të një mesazhi të fshehur. Përndryshe, çdokush që sheh mesazhin e fshehur mund të shohë edhe se kush janë marrësit. ID Kyçe Besueshmëri Anësore Emri S'ka çelësa të disponueshëm S'është përzgjedhur asnjë çelës Asnjë (Mos nënshkruani) Programth për fshehjen OpenPGP Mesazhet e tjera të ofruara nga GnuPG: Produkti i GnuPG: Përzgjidhni marrësit: Nënshkruajeni mesazhin si: Nënshkruajeni/Fshiheni Tabelën e Shënimeve me _Çelësa Publikë Gjendja Tabelën e Shënimeve nuk përmban të dhëna hyrëse të vlefshme. Çelësi i përzgjedhur, që vijoj, s'është plotësisht i besueshëm: Çelësat e përzgjedhur, që vijojnë, s'janë plotësisht të besueshëm: Për këtë arsye operacioni s'mund të kryhet. Besueshmëri e Skajshme Besueshmëri e Panjohur ID e përdoruesit ID-të e përdoruesve Jeni duke dalë nga OpenPGP Applet. Jeni i sigurt? Ju duhet të përzgjidhni një çelës personal për të shënuar mesazhin, apo disa çelësa publikë për ta fshehur mesazhin, ose të dyja. Ju nevojitet një çelës personal për të shënuar mesazhin ose një çelës publik për ta fshehur atë. _Deshifroni/Verifikoni Tabelën e Shënimeve _Administroni Çelësat _Hapeni Redaktuesin e Tekstit 