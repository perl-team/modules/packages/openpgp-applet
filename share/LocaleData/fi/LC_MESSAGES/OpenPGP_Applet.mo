��    &      L  5   |      P     Q     W     c  a   |  "   �            
             *     8  �   H     �     �     �     �               &  !   @     b     s     �  (   �     �  0   �  c   �  ,   \     �     �     �  3   �  g   �  L   U     �     �     �  �  �     h	     o	     	  �   �	  &   4
     [
     b
     p
     �
     �
     �
  �   �
     j     x     �     �     �      �     �     �          !     9  <   W     �  3   �  j   �  .   8     g       -   �  2   �  �   �  h   �  "   �          !                             &          $       	                  "                                              %               
                              #   !                    About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Jorma Karvonen <karvonen.jorma@gmail.com>
Language-Team: Finnish (http://www.transifex.com/otf/torproject/language/fi/)
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Tietoa Valitse avaimet Luotatko näihin avaimiin? Luotatko tähän aivaimeen tarpeeksi käyttääksesi sitä joka tapauksessa? Luotatko näihin avaimiin tarpeeksi käyttääksesi niitä joka tapauksessa? Salakirjoita leikepöytä _Salasanalla Lopeta Sormenjälki: Täysi luotettavuus. GnuPG-virhe GnuPG-tulokset Piilota vastaanottajat Piilota salakirjoitetun viestin kaikkien vastaanottajien käyttäjätunnisteet. Muuten kaikki, jotka näkevät salakirjoitetun viestin, voivat nähdä ketkä ovat vastaanottajat. Avaintunniste Marginaalinen luottamus Nimi Avaimia ei saatavilla Ei valittuja avaimia Ei mitään (älä allekirjoita) OpenPGP-salakirjoitussovelma Muut GnuPG-viestit: GnuPG-tuloste: Valitse vastaanottajat: Allekirjoita viesti nimellä: Allekirjoita/salakirjoita leikepöytä julkisilla _avaimilla Tila Leikepöytä ei sisällä kelvollista syötetietoa. Seuraava valittu avain ei ole kokonaan luotettu: Seuraavat valitut avaimet eivät ole kokonaan luotettuja: Tämän vuoksi tehtävää ei voida suorittaa. Rajoittamaton luottamus Tuntematon luotettavuus. Käyttäjätunnisteet: Käyttäjätunnisteet: Olet poistumassa OpenPGP sovelmasta. Oletko varma? Sinun on valittava yksityinen avain viestin allekirjoitukseen, tai jonkun julkisista avaimista viestin salakirjoittamiseen, tai molemmat. Tarvitset yksityisen avaimen viestin allekirjoitukseen tai julkisen avaimen viestin salakirjoittamiseen. _Pura/todenna leikepöydän salaus _Hallitse avaimia _Avaa tekstinmuokkain 