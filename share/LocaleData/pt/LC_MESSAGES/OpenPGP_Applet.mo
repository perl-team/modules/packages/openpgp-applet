��    &      L  5   |      P     Q     W     c  a   |  "   �            
             *     8  �   H     �     �     �     �               &  !   @     b     s     �  (   �     �  0   �  c   �  ,   \     �     �     �  3   �  g   �  L   U     �     �     �  �  �     g	     m	     �	  n   �	  >   
     D
     I
     ]
  
   q
     |
     �
  �   �
     _     k     �      �     �     �  *   �               1     M  @   f     �  6   �  |   �  4   b     �     �  &   �  A   �  x   1  h   �  1        E     S                             &          $       	                  "                                              %               
                              #   !                    About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2017-09-22 17:51+0000
Last-Translator: Manuela Silva <manuela.silva@sky.com>
Language-Team: Portuguese (http://www.transifex.com/otf/torproject/language/pt/)
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Sobre Escolher as chaves Confia nestas chaves? Confia mesmo nesta chave para a utilizar mesmo assim? Confia mesmo nestas chaves para as utilizar mesmo assim? Encriptar Área de Transferência com _Frase de Palavra-passe  Sair Assinatura digital: Fidedignidade total Erro GnuPG Resultados do GnuPG Ocultar destinatários Esconda os IDs de utilizador de todos os destinatários de uma mensagem encriptada. De outra forma qualquer pessoa que veja a mensagem encriptada pode ver quem são os destinatários. ID da chave Fidedignidade marginal Nome Não existem chaves disponíveis Nenhuma chave selecionada Nenhum (não assinar) Míni aplicação de encriptação OpenPGP Outras mensagens do GnuPG: Resultado do GnuPG Seleccionar destinatários: Assinar a mensagem como: Assinar/Encriptar a Área de Transferência com Chaves_Públicas Estado A área de transferência não contém dados válidos. A seguinte chave selecionada não é totalmente fidedigna: As seguintes chaves selecionadas não são totalmente fidedignas: Desta maneira a operação não pôde ser realizada: Fidedignidade máxima Fidedignidade desconhecida ID do utilizador: ID(s) do utilizador: Está prestes a sair da Míni Aplicação OpenPGP. Tem a certeza? Tem de seleccionar ou uma chave privada para assinar a mensagem, ou chaves privadas para encriptar a mensagem, ou ambas. É necessário uma chave privada para assinar mensagens, ou uma chave pública para encriptar mensagens. _Desencriptar/Verificar a Área de Transferência _Gerir Chaves Abrir edit_or de texto 