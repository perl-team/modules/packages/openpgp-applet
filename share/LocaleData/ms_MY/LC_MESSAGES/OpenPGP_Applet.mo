��    &      L  5   |      P     Q     W     c  a   |  "   �            
             *     8  �   H     �     �     �     �               &  !   @     b     s     �  (   �     �  0   �  c   �  ,   \     �     �     �  3   �  g   �  L   U     �     �     �  �  �     `	     h	     t	  ;   �	  +   �	     �	  	    
     

     
     (
     8
  �   J
     �
     �
     �
     �
          !     <  +   U     �     �     �  5   �     �  4   �  3   '  (   [     �     �     �  <   �  �     h   �     �          *                             &          $       	                  "                                              %               
                              #   !                    About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2017-12-06 16:26+0000
Last-Translator: abuyop <abuyop@gmail.com>
Language-Team: Malay (Malaysia) (http://www.transifex.com/otf/torproject/language/ms_MY/)
Language: ms_MY
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 Perihal Pilih kunci Anda percaya pada kunci ini? Anda percaya kunci ini secukupnya untuk menggunakannya jua? Sulitkan Papan Keratan dengan _Frasa Laluan Keluar Cap Jari: Kepercayaan Penuh Ralat GnuPG Keputusan GnuPG Sembunyi penerima Sembunyi ID pengguna bagi semua penerima mesej tersulit. Melainkan sesiapa yang dapat lihat mesej tersulit juga dapat mengetahui penerimanya. ID Kunci Kepercayaan Kecil Nama Tiada kunci tersedia Tiada kunci dipilih Tiada (Jangan tandatangan) Aplet penyulitan OpenPGP Lain-lain mesej yang disediakan oleh GnuPG: Output GnuPG: Pilih penerima: Daftar mesej sebagai: Tandatangan/Sulitkan Papan Keratan dengan _Kunci Awam Status Papan keratan tidak mengandungi data input yang sah. Kunci terpilih berikut tidak sepenuhnya dipercayai: Oleh itu operasi tidak boleh dijalankan. Kepercayaan Sepenuhnya Kepercayaan Tidak Diketahui ID Pengguna: Anda akan keluar dari Aplet OpenPGP. Anda pasti mahu keluar? Anda mesti pilih satu kunci persendirian untuk menandatangani mesej, atau sesetengah kunci awam yang dapat menyulitkan mesej, ataupun kedua-duanya. Anda perlukan satu kunci persendirian untuk menandatangan mesej atau kunci awam untuk menyulitkan mesej. _Nyahsulit/Sahkan Papan Keratan _Urus Kunci _Buka Penyunting Teks 