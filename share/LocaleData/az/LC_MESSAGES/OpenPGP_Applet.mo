��    $      <  5   \      0     1     7     C  a   \  "   �     �     �  
   �     �     
       �   (     �     �     �     �     �     �       !         B     S     f  (   w     �  0   �  c   �  ,   <     i     x     �  g   �  L        N     h  �  u  	   �     	     	  �   0	  6   �	     
     
  	   
     #
     2
     E
  �   ^
       	   !     +     0     =     P     i  5   �     �     �     �  ?   �     =  8   D  P   }  5   �            ,   '  |   T  ]   �  0   /     `                                        #       	                  !                                              $               
                              "                        About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2014-12-30 17:30+0000
Last-Translator: E <ehuseynzade@gmail.com>
Language-Team: Azerbaijani (http://www.transifex.com/projects/p/torproject/language/az/)
Language: az
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Haqqında Açarları seç Bu açarlara inanırsan? Heç nəyə baxmayaraq bu açarı istifadə edəcək qədər ona inanırsan? Heç nəyə baxmayaraq bu açarları istifadə edəcək qədər onlara inanırsan? Mübadilə Buferini _Şifrə İfadəsi ilə şifrələ Çıx Barmaq izi: Tam İnam GnuPG xətası GnuPG nəticələr Qəbul ediciləri gizlə Şifrələnmiş mesajı qəbul edənlərin hamısının istifadəçi ID-ni gizlə. Əks halda, şifrələnmiş mesajı görən hər hansı şəxs onu qəbul edəni də görə bilər. Açar ID-si Son İnam Name Açar yoxdur Açar seçilməyib Heç biri (Giriş etmə) OpenPGP şifrələnmə apleti GnuPG tərəfindən təqdim edilmiş başqa mesajlar: GnuPG nəticəsi: Qəbul ediciləri seç: Mesajı belə imzala: İctimai _Açarlarla Mübadilə Buferinə/i Daxil Ol/Şifrələ Status Mübadilə buferində düzgün giriş məlumatı yoxdur. Bu seçilmiş açar tam inamlı deyil: Bu seçilmiş açarlar tam inamlı deyil: Buna görə də əməliyyat yerinə yetirilə bilmir. Başlıca İnam Bilinməyən İnam İstifadəçi ID-si: İstifadəçi ID-ləri: Mesajı imzalamaq üçün şəxsi açar, şifrələmək üçün hər hansı ictimai açar, ya da hər ikisini seçməlisən. Mesajı imzalamaq üçün şəxsi, şifrələmək üçün isə ictimai açara ehtiyacın var. Mübadilə Buferin/i _Şifrəsini Aç/Təsdiqlə Açarları _İdarə et 