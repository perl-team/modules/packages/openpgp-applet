��    $      <  5   \      0     1     7     C  a   \  "   �     �     �  
   �     �     
       �   (     �     �     �     �     �     �       !         B     S     f  (   w     �  0   �  c   �  ,   <     i     x     �  g   �  L        N     h  �  u     H	     P	     a	  �   {	  -   c
     �
     �
     �
     �
     �
     �
  �   �
  	   r     |     �     �     �     �     �  #   �                4  2   M     �  2   �  �   �  "   �     �     �  4   �  {     S   �  %   �     
                                        #       	                  !                                              $               
                              "                        About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2015-02-12 14:52+0000
Last-Translator: runasand <runa.sandvik@gmail.com>
Language-Team: Slovenian (Slovenia) (http://www.transifex.com/projects/p/torproject/language/sl_SI/)
Language: sl_SI
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Vizitka Izberite ključe Ali zaupate tem ključem? Ali dovolj zaupate temu ključu, da ga vseeno uporabite? Ali dovolj zaupate tema ključema, da ju vseeno uporabite? Ali dovolj zaupate tem ključem, da jih vseeno uporabite? Ali dovolj zaupate tem ključem, da jih vseeno uporabite? Šifriranje odložišča z _Geslom za ključe Izhod Prstni odtis: Zaupanja vreden Skrbnik GnuPG napaka GnuPG rezultati Skrij prejemnike Skrij ID uporabnika vsem prejemnikom šifriranega sporočila. Drugače lahko vsak, ki šifrirano sporočilo vidi, ve kdo je prejemnik. Ključ ID Mejni Skrbnik Ime Ni uporabnega ključa Ključ ni izbran Nobeden (Brez podpisa) Odpri PGP šifrirni programček Druga sporočila ponujena od GnuPG: Izhod iz GnuPG: Izbira prejemnikov: Podpiši sporočilo kot: Vpis / Šifriranje odložišča z javnimi _Ključi Stanje Odložišče ne vsebuje veljavnih vhodnih podatkov Sledeči izbran ključ ni vreden popolnega zaupanja: Sledeča izbrana ključa nista vredna popolnega zaupanja: Sledeči izbrani ključi niso vredni popolnega zaupanja: Sledeči izbrani ključi niso vredni popolnega zaupanja: Zato ni mogoče izvesti operacijo. Dokončni Skrbnik Neznan Skrbnik Uporbnik ID: Uporabnika ID: Uporabniki ID: User IDs: Izbrati morate zasebni ključ za podpisovanje sporočila, ali kateri javni ključi za šifriranje sporočila, ali pa oboje. Rabite zasebni ključ za podpis sporočila ali javni ključ za šifriranje le tega. _Dešifriranje/Preverite Odložišče _Upravljanje s Ključi 