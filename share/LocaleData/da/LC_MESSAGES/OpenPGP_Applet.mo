��    &      L  5   |      P     Q     W     c  a   |  "   �            
             *     8  �   H     �     �     �     �               &  !   @     b     s     �  (   �     �  0   �  c   �  ,   \     �     �     �  3   �  g   �  L   U     �     �     �  o  �     K	     N	     \	  �   y	  '   
     5
     <
     J
  
   ]
     h
     y
  �   �
  	             .     3     O     d     y     �     �     �     �  6   �     "  3   )  W   ]  (   �     �     �       M   $  ~   r  e   �  #   W     {     �                             &          $       	                  "                                              %               
                              #   !                    About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2018-01-11 10:30+0000
Last-Translator: scootergrisen
Language-Team: Danish (http://www.transifex.com/otf/torproject/language/da/)
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Om Vælg nøgler Stoler du på disse nøgler? Stoler du tilstrækkeligt på disse nøgler til at anvende dem alligevel? Stoler du tilstrækkeligt på disse nøgler til at anvende dem alligevel? Kryptér udklipsholder med _adgangskode Afslut Fingeraftryk: Fuld troværdighed GnuPG fejl GnuPG resultater Skjul modtagere Skjul bruger ID'erne af alle modtagere af en krypteret besked. Ellers kan alle der ser den krypterede besked se hvem modtagerne er. Nøgle ID Marginal troværdighed Navn Ingen nøgler tilgængelige Ingen nøgler valgte Ingen (signér ikke) OpenPGP krypteringsprogram Andre beskeder udbudt af GnuPG: GnuPG's output: Vælg modtagere: Signér beskeden som: Signér/kryptér udklipsholderen med offentlig _nøgle Status Udklipsholderen indeholder ikke gyldigt input data. Stoler ikke fuldt på de følgende nøgler: Stoler ikke fuldt på de følgende nøgler: Derfor kan denne handling ikke udføres. Ultimativ troværdighed Ukendt troværdighed Bruger-ID: Bruger-ID'er: Du afslutter nu OpenPGP programmet. Er du helt sikker på at du vil afslutte? Du skal vælge en privat nøgle til at signere beskeden, eller nogle offentlige nøgler til at kryptere beskeden, eller begge. Du skal bruge en privat nøgle til at signere beskeder eller en offentlig nøgle til at kryptere dem. _Dekryptér/verificer udklipsholder _Administrer nøgler _Åbn tekstredigering 