��    &      L  5   |      P     Q     W     c  a   |  "   �            
             *     8  �   H     �     �     �     �               &  !   @     b     s     �  (   �     �  0   �  c   �  ,   \     �     �     �  3   �  g   �  L   U     �     �     �  �  �     p	     s	     �	  �   �	  *   
     J
     R
     a
  
   m
     x
     �
  �   �
  
   !     ,     <     A     `     w     �     �     �     �     �  .        0  /   7  m   g  %   �     �            =   3  ~   q  n   �     _     z     �                             &          $       	                  "                                              %               
                              #   !                    About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Allan Nordhøy <epost@anotheragency.no>
Language-Team: Norwegian Bokmål (http://www.transifex.com/otf/torproject/language/nb/)
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Om Velg nøkler Stoler du på disse nøklene? Har du nok tiltro til denne nøkkelen til å bruke den likevell? Har du nok tiltro til disse nøklene til å bruke dem likevell? Krypter utklippstavlen med _passordsekvens Avslutt Fingeravtrykk: Full tillit GnuPG-feil GnuPG resultater Gjem mottakere: Gjem bruker-ID-ene til alle mottakere av en kryptert melding. Hvis ikke kan alle som ser den krypterte meldingen se hvem mottakerne er. Nøkkel-ID Marginal tillit Navn Ingen nøkler er tilgjengelige Ingen nøkler er valgt Ingen (Ikke signer) OpenPGP krypterings-miniprogram Andre meldinger gitt av GnuPG: Ytelse av GnuPG: Velg mottakere: Signer meldingen som: Signer/krypter utklipp med offentlige _nøkler Status Utklippstavlen inneholder ikke gyldige inndata. Følgende valgte nøkkel er ikke fullstending betrodd: Følgende valgte nøkler er ikke fullstending betrodd: Derfor kan ikke operasjonen utføres. Fullstendig tillit Ukjent tillit BrukerID: BrukerID-er: Du er i ferd med å avslutte OpenPGP-miniprogrammet. Bekreft. Du må velge en privat nøkkel for å signere meldingen, eller noen offentlige nøkler for å krypere den, eller begge deler.  Du trenger en privat nøkkel for å signere meldinger eller en offentlig nøkkel for å dekryptere meldinger.  _Dekrypter/bekreft utklipp _Håndter nøkler _Åpne tekstbehandler 