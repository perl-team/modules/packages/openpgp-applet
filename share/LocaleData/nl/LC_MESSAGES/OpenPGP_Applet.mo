��    &      L  5   |      P     Q     W     c  a   |  "   �            
             *     8  �   H     �     �     �     �               &  !   @     b     s     �  (   �     �  0   �  c   �  ,   \     �     �     �  3   �  g   �  L   U     �     �     �  �  �     e	     j	     x	  l   �	  #    
     $
     -
     ;
  
   O
     Z
     k
  �   ~
  
   !     ,     A     F     `     z     �  /   �     �     �       0   #     T  )   [  �   �  +        4     F     Z  /   v  �   �  t   :     �     �     �                             &          $       	                  "                                              %               
                              #   !                    About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2018-02-22 21:59+0000
Last-Translator: Anne Mügge <info@numijneigenwebsite.nl>
Language-Team: Dutch (http://www.transifex.com/otf/torproject/language/nl/)
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Over Kies sleutels Vertrouw je deze sleutels? Vertrouw je deze sleutel genoeg om toch te gebruiken? Vertrouw je deze sleutels genoeg om toch te gebruiken? Versleutel Klembord met _Wachtwoord Sluit af Vingerafdruk: Volledig Vertrouwen GnuPG-fout GnuPG resultaten Verberg ontvangers Verberg de gebruikers-ID's van alle ontvangers van een versleuteld bericht. Anders kan iedereen die het versleuteld bericht kan lezen zien wie de ontvangers zijn. Sleutel ID Marginaal Vertrouwen Naam Geen sleutels beschikbaar Geen sleutel geselecteerd Geen (niet ondertekenen) OpenPGP versleutel applicatie  Andere berichten die door GnuPG gegeven worden: Uitvoer van GnuPG: Selecteer ontvangers: Onderteken bericht als: Teken/Versleutel Klembord met Publieke _Sleutels Status Het klembord bevat geen geldige gegevens. De volgende geselecteerde sleutel is niet helemaal betrouwbaar: De volgende geselecteerde sleutels zijn niet helemaal betrouwbaar: Daarom kan de actie niet uitgevoerd worden. Ultiem Vertrouwen Onbekend Vertrouwen GebruikerID: GebruikerID's: Je verlaat de OpenPGP-applicatie. Ben je zeker? Je moet een private sleutel selecteren om het bericht mee te ondertekenen, ofwel enkele publieke sleutels om het bericht te versleutelen, of beide. Je hebt een private sleutel nodig om berichten te ondertekenen of een publieke sleutel om berichten te versleutelen. Ontcijfer/Controleer Klembord Beheer Sleutels _text editor openen 