��    &      L  5   |      P     Q     W     c  a   |  "   �            
             *     8  �   H     �     �     �     �               &  !   @     b     s     �  (   �     �  0   �  c   �  ,   \     �     �     �  3   �  g   �  L   U     �     �     �  k  �     G	     T	     e	  w   	  "   �	     
     
     ,
     B
     O
     _
  �   t
  	             5     :     U     p  #   �      �     �     �     �  .        >  0   D  y   u  /   �          6     Q  7   g  �   �  \   ,     �     �     �                             &          $       	                  "                                              %               
                              #   !                    About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Random_R
Language-Team: Italian (http://www.transifex.com/otf/torproject/language/it/)
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Informazioni Scegli le chiavi Fidarsi di queste chiavi? Ci si fida di questa chiave abbastanza per usarla comunque? Ci si fida di queste chiavi abbastanza per usarle comunque? Cripta gli appunti con _Passphrase Esci Fingerprint: Attendibilità totale Errore GnuPG Risultati GnuPG Nascondi destinatari Nascondi l'ID utente di tutti i destinatari di un messaggio criptato. Altrimenti chiunque leggerà il messaggio criptato potrà vedere chi sono i destinatari. ID chiave Attendibilità parziale Nome Nessuna chiave disponibile Nessuna chiave selezionata Nessuno (non firmare) Applet per crittografia con OpenPGP Altri messaggi forniti da GnuPG: Risultato di GnuPG: Seleziona destinatari: Firma il messaggio come: Firma/Cripta gli appunti con _Chiavi pubbliche Stato Gli appunti non contengono dati di input validi. La seguente chiave selezionata non è completamente fidata: Le seguenti chiavi selezionate non sono completamente fidate: Pertanto l'operazione non può essere eseguita. Attendibilità estrema Attendibilità sconosciuta ID utente: ID utenti: Stai cercando di chiudere l'applet OpenPGP. Sei sicuro? È necessario selezionare una chiave privata per firmare il messaggio, o alcune chiavi pubbliche per criptare il messaggio, oppure entrambi. È necessaria una chiave privata per firmare i messaggi o una chiave pubblica per criptarli. _Decripta/Verifica appunti _Gestisci chiavi _Apri l'editor testuale 