��    &      L  5   |      P     Q     W     c  a   |  "   �            
             *     8  �   H     �     �     �     �               &  !   @     b     s     �  (   �     �  0   �  c   �  ,   \     �     �     �  3   �  g   �  L   U     �     �     �  �  �     ]	     d	     q	  3   �	     �	     �	  	   �	     �	     �	     
     
  u   )
  	   �
     �
     �
     �
     �
     �
     �
          '     7     J  &   `     �     �  -   �     �     �            6     ]   U  <   �     �                                       &          $       	                  "                                              %               
                              #   !                    About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: YF <yfdyh000@gmail.com>
Language-Team: Chinese (China) (http://www.transifex.com/otf/torproject/language/zh_CN/)
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 关于 选择密钥 是否信任这些密钥？ 你是否足够信任这些密钥来使用它们？ 使用密码加密剪贴板(_P) 关闭 指纹： 完全信任 GnuPG 出错 GnuPG 结果 隐藏接收者 隐藏加密信息所有接收者的用户 ID，否则可看到加密信息的任何人都可查看谁是接收者。 密钥 ID 部分信任 名称 无可用密钥 未选择密钥 无（不签名） OpenPGP 加密小程序 GnuPG 提供的其他信息： GnuPG 输出： 选择接收者： 签名信息作为： 使用公钥签名/加密剪贴板(_K) 状态 剪贴板输入数据无效。 以下已选中的密钥未被完全信任： 因此无法执行操作。 最高信任 未知信任 用户 ID： 你将要退出 OpenPGP 加密小程序。确定吗？ 必须选择私钥签署信息，或者选择公钥加密信息，或者同时选择两者。 需要使用私钥签署信息或使用公钥加密信息。 解密/验证剪贴板(_D) 管理密钥(_M) 打开文本编辑器(_O) 