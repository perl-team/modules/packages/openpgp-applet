��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  �     
  �     �     �   �  �      	R     	s     	y     	�     	�     	�     	�   �  	�   	  
b     
l     
�     
�     
�     
�      
�   #  
�          "     8   *  M     x   *     d  �           0     J   $  c   /  �   r  �   X  +     �     �     � About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2017-09-22 17:51+0000
Last-Translator: Iban
Language-Team: Basque (http://www.transifex.com/otf/torproject/language/eu/)
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Honi buruz Gakoak aukeratu Gako hauetaz fidatzen zara? Gako honetaz behar bezain fidatzen zara nolanahi ere erabiltzeko? Gako hauetaz behar bezain fidatzen zara nolanahi ere erabiltzeko? Arbela enkriptatu Pasahitzarekin Irten Hatz-marka: Fidagarritasun Osoa GnuPG errorea GnuPG emaitzak Hartzaileak ezkutatu Enkriptatutako mezu baten hartzaile guztien IDak ezkutatu. Bestela, enkriptatutako mezua ikustea duen edonork ikusi dezake zeintzuk diren hartzaileak. Gako ID-a Fidagarritasun Marjinala Izena Ez dago gakorik. Aukeratutako gakorik ez Bat ere ez (Ez sinatu) OpenPGP enkriptaziorako applet-a GnuPG-k emandako beste mezu batzuk: GnuPGren irteera: Hartzaileak aukeratu: Mezua honela sinatu: Arbela Sinatu/Enkriptatu Gako _Puklikoekin Egoera Arbelak ez dauka baliozko sarrera daturik. Hurrengo gako aukeratua ez da guztiz fidagarria: Aukeratutako gako hauek ez dira guztiz fidagarriak: Beraz, eragiketa ezin da gauzatu Erabateko Fidagarritasuna Fidagarritasun Ezezaguna Erabiltzaile ID: Erabiltzaile ID-ak: OpenPGP Applet-tik irtetzera zoaz. Zihur zaude? Gako pribatu bat hautatu behar duzu mezua sinatzeko, edo gako publiko batzuk mezua enkriptatzeko; edo biak batera. Gako pribatu bat behar duzu mezuak sinatzeko edo gako publiko bat berauek enkriptatzeko. _Desenkriptatu/Egiaztatu Arbela Gakoak _Kudeatu _Ireki Testu Editorea 