��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  �       �     �     �   x  	   (  	�     	�   
  	�     	�     	�     	�     	�   �  	�     
�     
�     
�     
�     
�     
�               9     L     `   1  y     �   7  �   j  �   3  U     �     �   %  �   c  �   �  3   `  �           =     P About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2017-09-22 17:51+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic (http://www.transifex.com/otf/torproject/language/is/)
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n % 10 != 1 || n % 100 == 11);
 Um hugbúnaðinn Veldu lykla Treystir þú þessum lyklum? Treystirðu þessum lykli nægilega til að nota hann samt? Treystirðu þessum lyklum nægilega til að nota þá samt? Dulrita kli_ppispjald með lykilsetningu Hætta Fingrafar: Fullt traust GnuPG-villa Niðurstöður GnuPG Fela viðtakendur Fela notendaauðkenni allra viðtakenda dulritaðra skilaboða. Annars getur hver sá sem sér dulrituðu skilaboðin séð hverjir viðtakendurnir eru. Auðkenni lykils Traust á mörkunum Heiti Engir lyklar tiltækir Engir lyklar valdir Ekkert (ekki undirrita) OpenPGP dulritunarforrit Önnur skilaboð frá GnuPG: Frálag úr GnuPG: Veldu viðtakendur: Undirrita skilaboð sem: Undirrita/Dulrita _klippispjald með dreifilyklum Staða Klippispjaldið inniheldur ekki nein gild inntaksgögn. Eftirfarandi völdum lykli er ekki treyst til fulls: Eftirfarandi völdum lyklum er ekki treyst til fulls: Þar með er ekki hægt að framkvæma aðgerðina. Hámarks-traust Óþekkt traust Auðkenni notanda: Auðkenni notenda: Þú ert í þann mund að fara út úr OpenPGP-forritinu. Ertu viss um að þú viljir gera það? Þú verður að velja einkalykil til að undirrita skilaboðin, eða einhverja dreifilykla til að dulkóða skilaboðin, eða bæði. Þú þarft einkalykil til að undirrita skilaboð eða dreifilykil til að dulkóða skilaboð. _Afkóða/Sannvotta klippispjald Sýsla _með lykla _Opna textaritil 