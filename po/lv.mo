��       %     D              l     m     s        a  �   "  �          "   
  /     :     F     T   �  d     �     �                    0     B   !  \     ~     �     �   (  �     �   0  �   c     ,  x     �     �     �   g  �   L  =     �     �     �  �  �     �     �      �   �  �   !  	�     	�     	�     	�     	�     
     
   �  
)     
�      
�   	  
�     
�   #       5   "  H     k     �     �     �   -  �     �   '  �   �  &   0     !  D     f   0  �   �  �   ~  S     �     �     � About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2015-05-03 13:03+0000
Last-Translator: Ojārs Balcers <ojars.balcers@gmail.com>
Language-Team: Latvian (http://www.transifex.com/projects/p/torproject/language/lv/)
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 Par Izvēlēties atslēgas Vai uzticaties šīm atslēgām? Vai pietiekami uzticaties šai atslēgai, lai tomēr to lietotu? Vai pietiekami uzticaties šai atslēgai, lai tomēr to lietotu? Vai pietiekami uzticaties šīm atslēgām, lai tomēr tās lietotu? Šifrēt Clipboard ar _Passphrase Iziet Ciparvirkne: Pilns uzticamības līmenis GnuPG kļūda GnuPG rezultāti Slēpt saņēmējus Slēpt visu šifrētā ziņojuma saņēmēju lietotāju IDes. Savādāk, katrs, kas redz šifrēto ziņojumu var redzēt kas ir tā saņēmēji.  Atslēgas ID Mazzināms uzticamības līmenis Nosaukums Neviena atslēga nav pieejama Neviena no atslēgām nav atlasīta Nav (neparakstīt) OpenPGP šifrēšanas sīklietotne Citi GnuPG sniegti ziņojumi: GnuPG izvade: Atlasīt saņēmējus: Parakstīt ziņojumu kā: Parakstīt/šifrēt Clipboard ar Public _Keys Statuss Starpliktuvē nav derīgu ievades datu. Turpmāk norādītajām, izvēlētajām atslēgām nevar pilnībā uzticēties: Turpmāk norādītajai, izvēlētajai atslēgai nevar pilnībā uzticēties: Turpmāk norādītajām, izvēlētajām atslēgām nevar pilnībā uzticēties: Tādējādi operāciju nav iespējams izpildīt. Augstākais uzticamības līmenis Nezināms uzticamības līmenis Lietotāja IDes: Lietotāja ID: Lietotāja IDes: Jums vai nu jāatlasa privāta atslēga ar kuru parakstīt ziņojumu, vai arī kāda publiska atslēga, lai šifrētu ziņojumu, vai jāveic abas izvēles. Jums vai nu nepieciešama privāta atslēga, lai parakstītu ziņojumus, vai arī publiska atslēga, lai ziņojumus šifrētu. _Decrypt/Verify Clipboard _Manage Keys _Open teksta redaktors 