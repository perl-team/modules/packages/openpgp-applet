��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  �       �     �     �   �  	   )  	�   	  	�   
  	�     	�     
     
     
$   �  
6     
�     
�     
�     
�          )     A   #  \     �     �     �   5  �     �   -  �   �  $   '  �     �        X  !   4  z   x  �   f  (     �     �     � About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2017-09-22 17:51+0000
Last-Translator: Daša Matúšová
Language-Team: Slovak (Slovakia) (http://www.transifex.com/otf/torproject/language/sk_SK/)
Language: sk_SK
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 O nás Zvoliť kľúče Dôverujete týmto kľúčom? Dôverujete tomuto kľúču natoľko, aby ste ho použili? Dôverujete týmto kľúčom natoľko, aby ste ich použili? Dôverujete týmto kľúčom natoľko, aby ste ich použili? Šifrovať Schránku prístupovým heslom Ukončiť Odtlačok: Plná dôvera Chyba GnuPG Výsledok GnuPG Skryť príjemcov Skryť identifikátory všetkých príjemcov šifrovanej správy. V opačnom prípade môže ktokoľvek vidieť, kto je príjemcom šifrovanej správy. ID kľúča Minimálna dôvera Názov Žiadne dostupné kľúče Žiadne vybrané kľúče Žiadne (Nepodpisovať) OpenPGP šifrovací applet Ďalšie správy poskytnuté GnuPG: Výstup GnuPG: Vybrať príjemcov: Podpísať správu ako: Podpísať/zašifrovať Schránku verejným kľúčom Stav Schránka neobsahuje platné vstupné údaje. Nasledujúcemu vybranému kľúču nemožno úplne veriť: Nasledujúcim vybraným kľúčom nemožno úplne veriť: Nasledujúcim vybraným kľúčom nemožno úplne veriť: Preto nemohla byť operácia vykonaná. Neobmedzená dôvera Nejasná dôvera Identifikátor užívateľa: Identifikátory užívateľa: Identifikátory užívateľa: Chystáte sa ukončiť OpenPGP applet. Ste si istý? Musíte vybrať súkromný kľúč, ktorým podpíšte správu, verejné kľúče na zašifrovanie správy, alebo oboje. Potrebujete súkromný kľúč na podpísanie správy alebo verejný kľúč na zašifrovanie správy. _Dešifrovať/overiť Schránku _Spravovať kľúče _Otvoriť textový editor 