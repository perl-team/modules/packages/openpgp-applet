��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  �       �   0  �   N  �   �  	@   U  	�     
     
$     
7     
V     
e   /  
v  Q  
�     �   ,       9   -  @   '  n   !  �   !  �   E  �           2   3  R   a  �   
  �   4  �   b  (   9  �   +  �      �   !     l  4    �   �  �   8  �     �   '  � About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Mohsen Eghbal <Eghbalmohsen@gmail.com>
Language-Team: Persian (http://www.transifex.com/otf/torproject/language/fa/)
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 درباره کلید رمزنگاری را انتخاب کن آیا به این کلیدهای رمزنگاری اطمینان دارید؟ آیا به اندازی کافی به این کلیدها اطمینان دارید تا به هر حال استفاده شوند؟ کلیپ برد را به وسیله یک عبورواژه رمزگذاری کنید. خروج اثر انگشت: قابل اطمینان است خطای GnuPG نتایج GnuPG دریافت کننده ها را مخفی کن شناسه کاربری تمام دریافت کننده‌های یک پیغام رمزنگاری شده را پنهان کن. در غیر اینصورت هر کسی که این پیغام رمزنگاری شده را دریافت می کند، می تواند بفهمد چه کسان دیگری آن را دریافت کرده اند. کلید-شناسه به سختی قابل اطمینان است نام هیچ کلیدی در دست‌رس نیست کلیدی انتخاب نشده است هیچ کدام (امضا نکن) اپلت رمزنگاری OpenPGP سایر پیغام‌هایی که GnuPG ارائه کرده است: خروجی GnuPG: دریافت کننده‌ها: پیام را به این عنوان امضا کن: کلیپ برد را به وسیله یک کلید عمومی رمزگذاری/امضا کنید. وضعیت کلیپ‌برد دادهٔ معتبری ندارد کلیدهای انتخاب شده‌ی زیر کاملاً قابل اطمینان نیستند:  بنابراین عملیات قابل اجرا نیست. کاملاً قابل اطمینان است قابل اطمینان نیست شناسه‌های کاربری: شما قصد خروج از اپلت OpenPGP را دارید. از این کار اطمینان دارید؟ با انتخاب یک کلید خصوصی می‌توانید این پیام را امضا و با انتخاب کلید عمومی می‌توانید آن‌را رمزنگاری کنید؛ استفاده از هر دو مورد نیز امکان‌پذیر است. شما یک کلید خصوصی نیاز دارید تا پیام‌ها را امضا کنید و یا یک کلید عمومی نیاز دارید تا بتوانید پیام‌ها را رمزنگاری کنید. رمزگشایی/تایید امضای کلیپ‌برد مدیریت کلید‌ها _بازکردن ویرایشگر متن 