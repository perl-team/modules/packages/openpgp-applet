��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  �     
  �     �     �   �  	     	�     
	     
     
     
%     
1     
A   �  
U   
  
�     
�                2     S     m   "  �     �     �     �   7  �        ,  &   �  S   &       /     D   )  W   5  �   h  �   c        �     �     � About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Waseihou Watashi <waseihou@gmail.com>
Language-Team: Czech (http://www.transifex.com/otf/torproject/language/cs/)
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 O aplikaci Vybrat klíče Důvěřujete těmto klíčům? Věříte tomuto klíči dostatečně na to, abyste ho přesto použili? Věříte těmto klíčům dostatečně na to, abyste je přesto použili? Věříte těmto klíčům dostatečně na to, abyste je přesto použili? Šifrovat schránku s _Heslem Konec Otisk: Plná důvěra Chyba GnuPG GnuPG výsledky Skrýtí příjemci Skrýt uživatelská jména všech příjemců šifrované zprávy. Jinak každý, kdo uvidí zašifrovanou zprávu můžete vidět, kdo jsou příjemci. ID klíče Marginální důvěra Jméno Žádné dostupné klíče Nebyly vybrány žádné klíče Žádný (Nepřipojovat)  OpenPGP šifrovací applet Další zprávy poskytnuté GnuPG: Výstup z GnuPG Označit příjemce: Podepsat zprávu jako: Přihlásit/Zašifrovat Schránku s Veřejnými_Klíči Status Schránka neobsahuje validní vstupní data. Následující vybraný klíč není plně důvěryhodný: Následující vybrané klíče nejsou plně důvěryhodné: Následující vybrané klíče nejsou plně důvěryhodné: Proto nemůže být operace provedena. Nekonečná důvěra Neznámá důvěra ID uživatele ID uživatele ID uživatele Chystáte se ukončit OpenPGP Applet. Jste si jistí? Musíte vybrat soukromý klíč na podepsání nebo veřejný klíč na zašifrování zprávy nebo oba. Potřebujete soukromý klíč na podepsání zpráv nebo veřejný klíč na zašifrování zpráv. _Dešifrovat/Ověřit Schránku _Spravovat Klávesy _Otevřít textový editor 