��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  C       	K     	W     	f   �  	}     
t     
�     
�     
�     
�     
�     
�   �  
�   	  �     �     �     �     �     �     �   )       5     B     V   7  p     �   &  �   �  �   5  ~     �     �   I  �   <  $   |  a   f  �     E     b     y About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Michał Nowak <nowak1michal@gmail.com>
Language-Team: Polish (http://www.transifex.com/otf/torproject/language/pl/)
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 O programie Wybierz klucze Czy ufasz tym kluczom? Czy wystarczająco ufasz temu kluczowi, aby z niego korzystać? Czy wystarczająco ufasz tym kluczom, aby z nich korzystać? Czy wystarczająco ufasz tym kluczom, aby z nich korzystać? Czy wystarczająco ufasz tym kluczom, aby z nich korzystać? Szyfruj schowek _Hasłem Zamknij Fingerprint: Pełne Zaufanie Błąd GnuPG Rezultaty GnuPG Ukryj odbiorców Ukryj IDs wszystkich odbiorców zaszyfrowanej wiadomości. W przeciwnym razie każdy kto będzie widział zaszyfrowaną wiadomość będzie także widział jej odbiorców. Klucz ID: Krańcowe Zaufanie Nazwa Brak kluczy Nie wybrano żadnych kluczy Żaden (nie podpisuj) Aplikacja szyfrowania OpenPGP Inne wiadomości dostarczone przez GnuPG: Wyniki GnuPG Wybierz odbiorców: Podpisz wiadomość jako: Podpisz/Szyfruj schowek przy pomocy Publicznych _Kluczy Status Schowek nie zawiera poprawnych danych. Wybrany klucz nie jest do końca zaufany: Wybrane klucze nie są do końca zaufane: Wybrane klucze nie są do końca zaufane: Wybrane klucze nie są do końca zaufane: W związku z tym operacja nie może zostać wykonana. Najwyższe Zaufanie Nieznane Zaufanie ID użytkownika: ID użytkowników: ID użytkowników: ID użytkowników: Zamierzasz wyjść z OpenPGP Applet. Czy jesteś tego pewny? Musisz wybrać klucz prywatny, aby podpisać wiadomość, lub jakiś klucz publiczny, aby zaszyfrować wiadomość, lub oba. Potrzebujesz klucza prywatnego, aby podpisywać wiadomości, lub klucza publicznego aby je szyfrować. _Odszyfruj/Weryfikuj schowek _Zarządzanie Kluczami _Otwórz Edytor Tekstowy 