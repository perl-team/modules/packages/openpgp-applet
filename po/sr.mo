��       $     <              \     ]     c     o   a  �   "  �             
       *     6     D   �  T     �     �     �     �                2   !  L     n          �   (  �     �   0  �   c     ,  h     �     �     �   g  �   L  -     z     �  �  �     �   !  �   7  �  M  	   I  
P   
  
�     
�     
�     
�     
�        �  +   '     %  :     `   *  g   /  �   (  �   *  �   4       K   "  b   '  �   _  �        M    8  h   J  �   '  �   #     �  8   �  �   �  �   C  )   &  m About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2015-03-29 20:33+0000
Last-Translator: JanaDi <dijana1706@gmail.com>
Language-Team: Serbian (http://www.transifex.com/projects/p/torproject/language/sr/)
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 О нама Изаберите кључеве Да ли верујете овим кључевима? Да ли довољно верујете овом кључу да би га ипак користили? Да ли довољно верујете овим кључевима да би их ипак користили? Да ли довољно верујете овим кључевима да би их ипак користили? Извршити енкрипцију клипборда са _Passphrase Излаз Отисак прста: Потпуно Поверење GnuPG  грешка GnuPG резултати Сакрити примаоце Сакријте идентификације свих корисника шифроване поруке. Иначе ће свако ко види шифровану поруку моћи да види ко су примаоци. Идентификација Кључа Маргинално Поверење Име Нема доступних кључева Ниједан кључ није изабран Ништа (Не потписивати) Отворити аплет PGP enkripcije Остале поруке које пружа GnuPG: Излаз од GnuPG: Изабрати примаоце: Потпишите поруку као: Извршити Енкрипцију/Потисивање клипборда са Public _Keys Статус Клипборд не садржи валидне улазне податке Следећем изабраном кључу се не може потпуно веровати: Следећим изабраним кључевима се не може потпуно веровати: Следећим изабраним кључевима се не може потпуно веровати: Самим тим, операција се не може извршити. Ултимативно Поверење Непознато Поверење Корисничка идентификација: Корисничке идентификације: Идентификације корисника: Морате изабрати приватни кључ за потписивање поруке или неке јавне кључеве да шифрујете поруку, или обоје. Треба вам приватни кључ за потписивање порука или јавни кључ за шифровање порука. _Декриптовати/Верификовати Клипборд _Управљање Кључевима 