# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Tails developers
# This file is distributed under the same license as the OpenPGP_Applet package.
#
# Translators:
# Joaquín Serna, 2017
# psss <facevedo@openmailbox.org>, 2016
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: tails@boum.org\n"
"POT-Creation-Date: 2017-08-05 15:07-0400\n"
"PO-Revision-Date: 2018-02-20 19:11+0000\n"
"Last-Translator: yamila gonzalez <yamilabaddouh@gmail.com>\n"
"Language-Team: Spanish (Argentina) (http://www.transifex.com/otf/torproject/"
"language/es_AR/)\n"
"Language: es_AR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: bin/openpgp-applet:160
msgid "You are about to exit OpenPGP Applet. Are you sure?"
msgstr "Estas a punto de abandonar el Applet de OpenPGP, ¿estas seguro?"

#: bin/openpgp-applet:172
msgid "OpenPGP encryption applet"
msgstr "Applet de cifrado OpenPGP"

#: bin/openpgp-applet:175
msgid "Exit"
msgstr "Salir"

#: bin/openpgp-applet:177
msgid "About"
msgstr "Acerca de"

#: bin/openpgp-applet:232
msgid "Encrypt Clipboard with _Passphrase"
msgstr "Cifrar el portapapeles con _Frase de contraseña"

#: bin/openpgp-applet:235
msgid "Sign/Encrypt Clipboard with Public _Keys"
msgstr "Cifrar y firmar el portapapeles con Clave _Pública"

#: bin/openpgp-applet:240
msgid "_Decrypt/Verify Clipboard"
msgstr "_Descifrar/Verificar el portapapeles"

#: bin/openpgp-applet:244
msgid "_Manage Keys"
msgstr "_Administrar Claves"

#: bin/openpgp-applet:248
msgid "_Open Text Editor"
msgstr "Abrir _Editor de Textos"

#: bin/openpgp-applet:292
msgid "The clipboard does not contain valid input data."
msgstr "El portapapeles no contiene datos de entrada válidos."

#: bin/openpgp-applet:337 bin/openpgp-applet:339 bin/openpgp-applet:341
msgid "Unknown Trust"
msgstr "Nivel de confianza Desconocido"

#: bin/openpgp-applet:343
msgid "Marginal Trust"
msgstr "Nivel de confianza Marginal"

#: bin/openpgp-applet:345
msgid "Full Trust"
msgstr "Nivel de confianza Completa "

#: bin/openpgp-applet:347
msgid "Ultimate Trust"
msgstr "Nivel de confianza Total"

#: bin/openpgp-applet:400
msgid "Name"
msgstr "Nombre"

#: bin/openpgp-applet:401
msgid "Key ID"
msgstr "ID de Clave"

#: bin/openpgp-applet:402
msgid "Status"
msgstr "Estado"

#: bin/openpgp-applet:433
msgid "Fingerprint:"
msgstr "Huella digital:"

#: bin/openpgp-applet:436
msgid "User ID:"
msgid_plural "User IDs:"
msgstr[0] "ID de usuario:"
msgstr[1] "IDs de usuarios:"

#: bin/openpgp-applet:465
msgid "None (Don't sign)"
msgstr "Ninguno (No firmar)"

#: bin/openpgp-applet:528
msgid "Select recipients:"
msgstr "Elija los destinatarios:"

#: bin/openpgp-applet:536
msgid "Hide recipients"
msgstr "Ocultar destinatarios"

#: bin/openpgp-applet:539
msgid ""
"Hide the user IDs of all recipients of an encrypted message. Otherwise "
"anyone that sees the encrypted message can see who the recipients are."
msgstr ""
"Ocultar los ID de usuario de todos los destinatarios de un mensaje cifrado. "
"De otra manera, cualquiera que pueda ver el mensaje cifrado puede ver "
"quienes son los destinatarios."

#: bin/openpgp-applet:545
msgid "Sign message as:"
msgstr "Firmar mensaje como:"

#: bin/openpgp-applet:549
msgid "Choose keys"
msgstr "Elija las claves"

#: bin/openpgp-applet:589
msgid "Do you trust these keys?"
msgstr "¿Tiene confianza en esas claves?"

#: bin/openpgp-applet:592
msgid "The following selected key is not fully trusted:"
msgid_plural "The following selected keys are not fully trusted:"
msgstr[0] "La siguiente clave no es completamente confiable: "
msgstr[1] "Las siguientes claves no son completamente confiables: "

#: bin/openpgp-applet:610
msgid "Do you trust this key enough to use it anyway?"
msgid_plural "Do you trust these keys enough to use them anyway?"
msgstr[0] ""
"¿Tiene suficiente confianza en esta clave para utilizarla igualmente?"
msgstr[1] ""
"¿Tiene suficiente confianza en estas claves para utilizarlas igualmente?"

#: bin/openpgp-applet:623
msgid "No keys selected"
msgstr "No hay claves seleccionadas"

#: bin/openpgp-applet:625
msgid ""
"You must select a private key to sign the message, or some public keys to "
"encrypt the message, or both."
msgstr ""
"Debe seleccionar una clave privada para firmar el mensaje, alguna clave "
"pública para cifrar el mensaje o ambas."

#: bin/openpgp-applet:653
msgid "No keys available"
msgstr "No hay claves disponibles"

#: bin/openpgp-applet:655
msgid ""
"You need a private key to sign messages or a public key to encrypt messages."
msgstr ""
"Necesita una clave privada para firmar mensajes, o una clave publica para "
"cifrar mensajes."

#: bin/openpgp-applet:783
msgid "GnuPG error"
msgstr "Error GnuPG"

#: bin/openpgp-applet:804
msgid "Therefore the operation cannot be performed."
msgstr "Por lo tanto, la operación no puede realizarse."

#: bin/openpgp-applet:854
msgid "GnuPG results"
msgstr "Resultados de GnuPG"

#: bin/openpgp-applet:860
msgid "Output of GnuPG:"
msgstr "Salida de GnuPG"

#: bin/openpgp-applet:885
msgid "Other messages provided by GnuPG:"
msgstr "Otros mensajes proporcionados por GnuPG"
