��       $     <              \     ]     c     o   a  �   "  �             
       *     6     D   �  T     �     �     �     �                2   !  L     n          �   (  �     �   0  �   c     ,  h     �     �     �   g  �   L  -     z     �  �  �     �     �   +  �   �  �   (  	�     	�     	�     	�     
     
     
0   �  
C     
�     
�     
�     
�          '     @   '  ]     �     �     �   5  �     �   2  �   �  0   '  �          #   A  ;   {  }   c  �     ]     { About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2014-04-30 09:10+0000
Last-Translator: runasand <runa.sandvik@gmail.com>
Language-Team: Welsh (http://www.transifex.com/projects/p/torproject/language/cy/)
Language: cy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1) ? 0 : (n==2) ? 1 : (n != 8 && n != 11) ? 2 : 3;
 Ynghylch Dewiswch allweddi Ydych chi'n ymddiried yn yr allweddi rhain? Ydych chi'n ymddirio digon yn yr allwedd hon i'w ddefnyddio? Ydych chi'n ymddirio digon yn yr allweddi hyn i'w ddefnyddio? Ydych chi'n ymddirio digon yn yr allweddi hyn i'w ddefnyddio? Ydych chi'n ymddirio digon yn yr allweddi hyn i'w ddefnyddio? Amgryptiwch y Clipfwrdd gydag _cyfrinair Gadael Olion Bysedd: Ymddiriediaeth llawn Gwall GnuPG Canlyniadau GnuPG Cyddiwch derbynwyr Cyddiwch yr IDau o phob derbynwr o neges amgryptiedig. Neu gall unrhyw un sy'n gweld y neges amlgryptiedig gallu gweld pwy yw'r derbynwyr. ID Allweddol Ymddiriediaeth ymylol Enw Ddim allweddi ar gael Ddim allweddi wedi'i ddewis Ddim (Peidiwch llofnodi) Rhaglennig amgryptio OpenPGP Negeseuon eraill a ddarperir gan GnuPG: Allbwn o GnuPG Dewiswch derbynwyr: Llofnodwch y neges fel: Llofnodi/Amgryptio'r Clipfwrdd efo Allweddi_Cyhoeddus Statws Nid yw'r clipfwrdd yn cynnwys data mewnbwn ddilys. Nid yw'r allwedd hon yn hollol ymddiriedig: Nid yw'r allweddi hyn yn hollol ymddiriedig: Nid yw'r allweddi hyn yn hollol ymddiriedig: Nid yw'r allweddi hyn yn hollol ymddiriedig: Felly, ni all yr gweithred fynd ymlaen. Ymddiriediaeth terfynol Ymddiriediaeth anhysbys ID Defnyddiwr: IDau Defnyddiwr: IDau Defnyddiwr: IDau Defnyddiwr: Maen rhaid i chithau dewis allwedd preifat i llofnodi'r neges, neu rhai allweddi gyhoeddus i amgryptio'r neges, neu'r ddwy. Rydych chi angen allwedd preifat i llofnodi negeseuon, neu allwedd gyhoeddus i amgryptio negeseuon. Dadgryptio/Gwirio'r Clipfwrdd Rheoli Allweddi 