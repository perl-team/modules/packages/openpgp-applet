��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  �       �     �   !  �   �  �   -  	y     	�     	�     	�     	�     	�     	�   �  
     
�     
�     
�     
�     
�                :     X     k     �   E  �     �   ;  �   |  $   2  �     �     �   #     6  %   �  \   �  �   +       �     � About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: bjorninou <bjorninou@gmail.com>
Language-Team: German (http://www.transifex.com/otf/torproject/language/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Über Schlüssel auswählen Vertrauen Sie diesen Schlüsseln? Vertrauen Sie diesem Schlüssel genug, um ihn dennoch zu verwenden? Vertrauen Sie diesen Schlüsseln genug, um sie dennoch zu verwenden? Zwischenablage mit _Passphrase verschlüsseln Beenden Fingerabdruck: Volles Vertrauen GnuPG-Fehler GnuPG-Ergebnisse Empfänger verstecken Verstecken Sie die Benutzerkennungen von allen Empfängern. Ansonsten weiß jeder, der die verschlüsselte Nachricht sieht, wer die Empfänger sind. Schlüsselkennung Geringfügiges Vertrauen Name Keine Schlüssel verfügbar Keine Schlüssel ausgewählt Keine (Nicht signieren) OpenPGP-Verschlüsselungs-Applet Andere Nachrichten von GnuPG: Ausgabe von GnuPG: Empfänger auswählen: Nachricht signieren als: Zwischenablage mit öffentlichem _Schlüssel signieren/verschlüsseln Status Die Zwischenablage beinhaltet keine gültigen Eingabedaten. Dem folgenden Schlüssel wird nicht komplett vertraut. Den folgenden ausgewählten Schlüsseln wird nicht komplett vertraut: Deshalb kann der Vorgang nicht ausgeführt werden. Unbegrenztes Vertrauen Unbekanntes Vertrauen Benutzererkennung Benutzerkennungen Sie sind dabei, OpenPGP zu verlassen. Sind Sie sicher? Sie müssen einen privaten Schlüssel auswählen um die Nachricht zu signieren, oder öffentliche Schlüssel um die Nachricht zu verschlüsseln. Oder beides. Sie benötigen private Schlüssel, um Nachrichten zu signieren oder einen öffentlichen Schlüssel um Nachrichten zu verschlüsseln. Zwischenablage _entschlüsseln/überprüfen Schlüssel _verwalten _Textbearbeitung öffnen 