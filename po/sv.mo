��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  �       �     �     �   �  �   $  	d     	�     	�     	�     	�     	�     	�   �  	�   	  
q     
{     
�     
�     
�     
�   $  
�   !  
�          2     C   +  \     �   (  �   \  �   &       <     K     Y   9  v   �  �   l  =     �     �     � About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2017-09-22 17:51+0000
Last-Translator: Jonatan Nyberg
Language-Team: Swedish (http://www.transifex.com/otf/torproject/language/sv/)
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Om Välj nycklar Litar du på dessa nycklar? Litar du tillräckligt på den här nyckeln för att använda den ändå? Litar du tillräckligt på de här nycklarna för att använda dem ändå? Kryptera urklipp med _lösenordsfras Avsluta Fingeravtryck: Full tillit Fel från GnuPG Resultat från GnuPG Dölj mottagare Dölj användar-IDn för alla mottagare av krypterade meddelanden. Annars kan vem som helst som ser meddelandet också se vilka mottagarna är. Nyckel-ID Marginell tillit Namn Inga nycklar tillgängliga Inga nycklar valda Ingen (signera inte) Panelprogram för OpenPGP-kryptering Andra meddelanden givna av GnuPG: Utmatning från GnuPG: Välj mottagare: Signera meddelandet som: Signera/kryptera urklipp med publik_nycklar Status Urklippet innehåller ingen giltig data. Följande vald nyckel är inte helt betrodd: Följande valda nycklar är inte helt betrodda: Därför kan inte åtgärden utföras. Ultimat tillit Okänd tillit Användar-ID: Användar-IDn: Du håller på att avsluta OpenPGP Applet. Är du säker? Du måste välja en privat nyckel för att signera meddelandet, eller några publika nycklar för att kryptera meddelandet, eller både och. Du behöver en privat nyckel för att signera meddelanden eller publik nyckel för att kryptera meddelanden. _Dekryptera/verifiera urklipp _Hantera nycklar _Öppna textredigerare 