��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  �       �     �     �   3  �   )  	     	7     	>     	N     	[     	g     	s   ~  	�     
     
     
     
     
/     
?     
N      
e     
�     
�     
�   $  
�     
�   *  
�   *       @     _     l     |   %  �   `  �   H       _     z     � About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Chi-Hsun Tsai
Language-Team: Chinese (Hong Kong) (http://www.transifex.com/otf/torproject/language/zh_HK/)
Language: zh_HK
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 關於 選取鎖匙 信任呢啲鎖匙嗎？ 可以信任呢啲鎖匙到足以使用佢哋嗎？ 使用_Passphrase對剪貼簿進行加密 離開 數碼指紋： 完全信任 GnuPG錯誤 GnuPG結果 隱藏收件者 隱藏加密郵件全部收件者嘅用戶識別碼。否則任何人見到加密嘅郵件都可以睇出邊個係收件者。 鎖匙ID 臨界信任 名稱 無可用鎖匙 未選取鎖匙 無(未簽署) OpenPGP加密小程式 由GnuPG提供嘅其他訊息： GnuPG嘅輸出： 選取收件者： 郵件簽名為： 使用公匙簽署╱加密剪貼簿 狀態 剪貼簿未包含有效嘅輸入資料。 下列所選嘅鎖匙未被完全信任： 因此唔能夠執行操作。 終極信任 不明嘅信任 用戶識別碼： 離開OpenPGP加密程式。確定？ 須選取私人鎖匙對郵件進行簽名，或一啲公匙嚟加密郵件，或兩者兼行。 須選取私人鎖匙對郵件進行簽名，或公匙嚟加密郵件。 _Decrypt╱驗證剪貼簿 _Manage鎖匙 _Open Text Editor 