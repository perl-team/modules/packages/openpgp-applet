# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Tails developers
# This file is distributed under the same license as the OpenPGP_Applet package.
#
# Translators:
# David Stepan <stepand@tiscali.cz>, 2015
# Mikulas Holy, 2017
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: tails@boum.org\n"
"POT-Creation-Date: 2017-08-05 15:07-0400\n"
"PO-Revision-Date: 2018-02-20 19:11+0000\n"
"Last-Translator: Waseihou Watashi <waseihou@gmail.com>\n"
"Language-Team: Czech (http://www.transifex.com/otf/torproject/language/cs/)\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: bin/openpgp-applet:160
msgid "You are about to exit OpenPGP Applet. Are you sure?"
msgstr "Chystáte se ukončit OpenPGP Applet. Jste si jistí?"

#: bin/openpgp-applet:172
msgid "OpenPGP encryption applet"
msgstr "OpenPGP šifrovací applet"

#: bin/openpgp-applet:175
msgid "Exit"
msgstr "Konec"

#: bin/openpgp-applet:177
msgid "About"
msgstr "O aplikaci"

#: bin/openpgp-applet:232
msgid "Encrypt Clipboard with _Passphrase"
msgstr "Šifrovat schránku s _Heslem"

#: bin/openpgp-applet:235
msgid "Sign/Encrypt Clipboard with Public _Keys"
msgstr "Přihlásit/Zašifrovat Schránku s Veřejnými_Klíči"

#: bin/openpgp-applet:240
msgid "_Decrypt/Verify Clipboard"
msgstr "_Dešifrovat/Ověřit Schránku"

#: bin/openpgp-applet:244
msgid "_Manage Keys"
msgstr "_Spravovat Klávesy"

#: bin/openpgp-applet:248
msgid "_Open Text Editor"
msgstr "_Otevřít textový editor"

#: bin/openpgp-applet:292
msgid "The clipboard does not contain valid input data."
msgstr "Schránka neobsahuje validní vstupní data."

#: bin/openpgp-applet:337 bin/openpgp-applet:339 bin/openpgp-applet:341
msgid "Unknown Trust"
msgstr "Neznámá důvěra"

#: bin/openpgp-applet:343
msgid "Marginal Trust"
msgstr "Marginální důvěra"

#: bin/openpgp-applet:345
msgid "Full Trust"
msgstr "Plná důvěra"

#: bin/openpgp-applet:347
msgid "Ultimate Trust"
msgstr "Nekonečná důvěra"

#: bin/openpgp-applet:400
msgid "Name"
msgstr "Jméno"

#: bin/openpgp-applet:401
msgid "Key ID"
msgstr "ID klíče"

#: bin/openpgp-applet:402
msgid "Status"
msgstr "Status"

#: bin/openpgp-applet:433
msgid "Fingerprint:"
msgstr "Otisk:"

#: bin/openpgp-applet:436
msgid "User ID:"
msgid_plural "User IDs:"
msgstr[0] "ID uživatele"
msgstr[1] "ID uživatele"
msgstr[2] "ID uživatele"

#: bin/openpgp-applet:465
msgid "None (Don't sign)"
msgstr "Žádný (Nepřipojovat) "

#: bin/openpgp-applet:528
msgid "Select recipients:"
msgstr "Označit příjemce:"

#: bin/openpgp-applet:536
msgid "Hide recipients"
msgstr "Skrýtí příjemci"

#: bin/openpgp-applet:539
msgid ""
"Hide the user IDs of all recipients of an encrypted message. Otherwise "
"anyone that sees the encrypted message can see who the recipients are."
msgstr ""
"Skrýt uživatelská jména všech příjemců šifrované zprávy. Jinak každý, kdo "
"uvidí zašifrovanou zprávu můžete vidět, kdo jsou příjemci."

#: bin/openpgp-applet:545
msgid "Sign message as:"
msgstr "Podepsat zprávu jako:"

#: bin/openpgp-applet:549
msgid "Choose keys"
msgstr "Vybrat klíče"

#: bin/openpgp-applet:589
msgid "Do you trust these keys?"
msgstr "Důvěřujete těmto klíčům?"

#: bin/openpgp-applet:592
msgid "The following selected key is not fully trusted:"
msgid_plural "The following selected keys are not fully trusted:"
msgstr[0] "Následující vybraný klíč není plně důvěryhodný:"
msgstr[1] "Následující vybrané klíče nejsou plně důvěryhodné:"
msgstr[2] "Následující vybrané klíče nejsou plně důvěryhodné:"

#: bin/openpgp-applet:610
msgid "Do you trust this key enough to use it anyway?"
msgid_plural "Do you trust these keys enough to use them anyway?"
msgstr[0] "Věříte tomuto klíči dostatečně na to, abyste ho přesto použili?"
msgstr[1] "Věříte těmto klíčům dostatečně na to, abyste je přesto použili?"
msgstr[2] "Věříte těmto klíčům dostatečně na to, abyste je přesto použili?"

#: bin/openpgp-applet:623
msgid "No keys selected"
msgstr "Nebyly vybrány žádné klíče"

#: bin/openpgp-applet:625
msgid ""
"You must select a private key to sign the message, or some public keys to "
"encrypt the message, or both."
msgstr ""
"Musíte vybrat soukromý klíč na podepsání nebo veřejný klíč na zašifrování "
"zprávy nebo oba."

#: bin/openpgp-applet:653
msgid "No keys available"
msgstr "Žádné dostupné klíče"

#: bin/openpgp-applet:655
msgid ""
"You need a private key to sign messages or a public key to encrypt messages."
msgstr ""
"Potřebujete soukromý klíč na podepsání zpráv nebo veřejný klíč na "
"zašifrování zpráv."

#: bin/openpgp-applet:783
msgid "GnuPG error"
msgstr "Chyba GnuPG"

#: bin/openpgp-applet:804
msgid "Therefore the operation cannot be performed."
msgstr "Proto nemůže být operace provedena."

#: bin/openpgp-applet:854
msgid "GnuPG results"
msgstr "GnuPG výsledky"

#: bin/openpgp-applet:860
msgid "Output of GnuPG:"
msgstr "Výstup z GnuPG"

#: bin/openpgp-applet:885
msgid "Other messages provided by GnuPG:"
msgstr "Další zprávy poskytnuté GnuPG:"
