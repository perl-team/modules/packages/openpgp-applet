��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  �       �     �     �   k  �   (  	I     	r   	  	w     	�     	�     	�     	�   �  	�     
u     
�     
�     
�     
�     
�      
�   %       ,     ?     \   4  s     �   2  �   h  �   *  J     u     �     �   J  �   u     Z  �   !  �     �      About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Adolfo Jayme-Barrientos
Language-Team: Catalan (http://www.transifex.com/otf/torproject/language/ca/)
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Quant a Tria les claus Confieu en aquestes claus? Confieu prou en aquesta clau per usar-la igualment?  Confieu prou en aquestes claus per usar-les igualment? Xifra el Portapapers amb la _Contrasenya Surt Empremta: Plena confiança Hi ha hagut un error de GnuPG Resultats del GnuPG Oculta els destinataris Oculta els IDs d'usuari de tots els destinataris del missatge xifrat. Altrament, qualsevol que vegi el missatge xifrat veurà qui són els destinataris. ID de la Clau Confiança marginal Nom No hi ha claus disponibles No hi ha cap clau seleccionada Cap (no signat) Miniaplicació de xifrat OpenPGP Altres missatges entregats per GnuPG: Sortida del GnuPG: Seleccioneu els destinataris Signa el missatge com: Signa/Xifra el Portapapers amb les _Claus Públiques Estat El portapapers no conté dades d'entrada vàlides. La clau sel·leccionada no és de plena confiança: Les claus seleccionades no són de plena confiança: Per tant, l'operació no es pot realitzar. Confiança fins al final Confiança desconeguda ID d'usuari: IDs d'usuari: Esteu a punt de sortir de la miniaplicació de l'OpenPGP. N'esteu segurs?  Heu de seleccionar una clau privada per signar el missatge o alguna clau pública per xifrar el missatge, o les dues. Necessiteu una clau privada per signar missatges o una clau pública per xifrar missatges. _Desxifra/Verifica el Portapapers _Gestiona les Claus _Obre l'editor de textos 