��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  �     	  �     �   "  �   |  �     	q   	  	�     	�     	�     	�     	�     	�   �  	�     
�     
�     
�   )  
�     
�          4     S     l     �     �   +  �     �   &  �   `  
   "  k     �     �   -  �   ;  �   �  #   n  �      &     G     \ About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2018-01-04 07:16+0000
Last-Translator: Kaya Zeren <kayazeren@gmail.com>
Language-Team: Turkish (http://www.transifex.com/otf/torproject/language/tr/)
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Hakkında Anahtarları seçin Bu anahtarlara güveniyor musunuz? Bu anahtara her durumda kullanacak kadar güveniyor musunuz? Bu anahtarlara her durumda kullanacak kadar güveniyor musunuz? Panoyu _Parola ile Şifrele Çıkış Parmak İzi: Tam Güvenirlik GnuPG hatası GnuPG sonuçları Alıcılar gizlensin Bu seçenek etkinleştirildiğinde, şifreli iletideki alıcılarının kullanıcı kimlikleri gizlenir. Aksi halde şifreli iletiyi görebilen herkes alıcıların da kimler olduğunu görebilir. Anahtar Kodu Az Bilinen Güvenirlik Ad Kullanılabilecek bir anahtar bulunamadı Henüz bir anahtar seçilmemiş Hiçbiri (İmzalama) OpenPGP şifreleme uygulaması Diğer GnuPG iletileri:  GnuPG çıktısı:  Alıcıları seçin: İletiyi şu olarak imzala: Panoyu Genel _Anahtarlarla İmzala/Şifrele Durum Panoda geçerli bir giriş verisi yok. Seçilmiş şu anahtara tamamen güvenilmiyor: Seçilmiş şu anahtarlara tamamen güvenilmiyor: Bu yüzden işlem yürütülemedi. Üstün Güvenirlik Bilinmeyen Güvenirlik Kullanıcı Kimliği: Kullanıcı Kimlikleri: OpenPGP Uygulamasından çıkmak üzeresiniz. Emin misiniz? İletiyi imzalamak için bir özel anahtar ya da iletiyi şifrelemek için bir genel anahtar seçmelisiniz. Ayrıca her ikisini de seçebilirsiniz. İletiyi imzalamak için bir özel anahtar ya da iletileri şifrelemek için bir genel anahtarınız olmalı.  Panonun Şifresini Aç/_Doğrula Anahtarları _Yönet _Metin Düzenleyicisini Aç 