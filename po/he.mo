��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  �     
  �     �   -  �   �  �   6  	�   
  	�     	�     	�     

     
     
.   �  
D     
          .      3     T     q   !  �   5  �     �     �   "     G  /     w   C  ~   �  �   5  J     �     �   -  �   J  �   �  (   �  �   $  i     �     � About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: ION
Language-Team: Hebrew (http://www.transifex.com/otf/torproject/language/he/)
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 אודות בחר מפתחות האם אתה בוטח במפתחות אלו? האם אתה בוטח מספיק במפתח זה כדי להשתמש בו בכל מקרה? האם אתה בוטח מספיק במפתחות אלו כדי להשתמש בהם בכל מקרה? הצפן לוח חיתוך עם ביטוי _סיסמה יציאה טביעת אצבע: אמון מלא שגיאת GnuPG תוצאות GnuPG הסתר מקבלים הסתר את זהויות המשתמש של כל מקבלי הודעה מוצפנת. אחרת, כל אחד שיראה את ההודעה המוצפנת יוכל גם לראות מי המקבלים. מזהה מפתח אמון זעום שם אין מפתחות זמינים לא נבחרו מפתחות כלום (אל תחתום) יישומון הצפנת OpenPGP הודעות אחרות המסופקות ע"י GnuPG: פלט של GnuPG: בחר מקבלים: חתום על הודעה בתור: חתום/הצפן לוח חיתוך עם _מפתחות ציבוריים מצב לוח החיתוך אינו מכיל נתוני קלט תקפים. המפתח הבא שנבחר אינו אמין במלואו: המפתחות הבאים שנבחרו אינם אמינים במלואם: לכן הפעולה אינה יכולה להתבצע. אמון מוחלט אמון בלתי ידוע זהות משתמש: זהויות משתמש: אתה עומד לצאת מיישומון OpenPGP. האם אתה בטוח? אתה חייב לבחור במפתח פרטי כדי לחתום על ההודעה, או במפתחות ציבוריים כלשהם כדי להצפין את ההודעה, או בשניהם. אתה צריך מפתח פרטי כדי לחתום על הודעות או מפתח ציבורי כדי להצפין הודעות. _פענח/וודא לוח חיתוך _נהל מפתחות _פתח עורך טקסט 