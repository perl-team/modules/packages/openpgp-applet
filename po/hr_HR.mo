��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �         	
     	     	   �  	=   !  	�     
     
      
.     
A     
O     
_   �  
q     
�          %     )     B     _      u   (  �     �     �     �   2       6   .  =   �  l   (       D     [   3  p   5  �   |  �   \  W     �     �     � About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Igor <lyricaltumor@gmail.com>
Language-Team: Croatian (Croatia) (http://www.transifex.com/otf/torproject/language/hr_HR/)
Language: hr_HR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 O Izaberi ključeve Vjerujete li ovim ključevima? Vjerujete li ovom ključu dovoljno da ga svejedno koristite? Vjerujete li ovom ključu dovoljno da ga svejedno koristite? Vjerujete li ovim ključevima dovoljno da ih svejedno koristite? Enkriptiraj Clipboard s _Lozinkom Izlaz Otisak prsta: Potpuno Povjerenje GnuPG greška GnuPG rezultati Sakrij primatelje Sakrij identifikacije svih primatelja enkriptirane poruke. U suprotnom svatko tko vidi enkriptiranu poruku može vidjeti tko su primatelji. Identifikacija ključa Rubno Povjerenje Ime Nema dostupnih ključeva Nijedan ključ nije izabran  Nijedan (Ne potpisuj) OpenPGP aplikacija za enkripciju Druge poruke pružene od strane GnuPG-a: GnuPG izlazne vrijednosti: Izaberi primatelje: Potpiši poruku kao: Potpiši/Šifriraj Clipboard s javnim _ključevima Status Clipboard ne sadrži važeće ulazne podatke.  Sljedećem odabranom ključu se ne vjeruje u potpunosti: Sljedećem odabranom ključu se ne vjeruje u potpunosti: Sljedećim odabranim ključevima se ne vjeruje u potpunosti: Stoga operacija ne može biti izvršena. Ultimativno Povjerenje Nepoznato Povjerenje Korisnički ID: Korisnički ID: Korisnički ID-evi: Izaći ćete iz OpenPGP aplikacije. Jeste li sigurni? Morate izabrati privatni ključ da biste potpisali poruku, ili neke javne ključeve da biste enkriptirali poruku, ili oboje. Trebate privatni ključ da biste potpisali poruke ili javni ključ da biste ih enkriptirali. _Dešifriraj/Potvrdi Clipboard _Upravljaj ključevima _Otvori uređivač teksta 