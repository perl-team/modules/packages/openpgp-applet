��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  �       �     �   $  �   B  �   '  	0     	X     	_     	k     	}     	�     	�   �  	�     
E     
N     
c     
h     
�     
�     
�     
�     
�     
�     	   /       O   4  V   7  �   1  �     �          %   6  2   w  i   Z  �     <     Z     h About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Robert Dafis <robertdafis@gmail.com>
Language-Team: Indonesian (http://www.transifex.com/otf/torproject/language/id/)
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 Tentang Pilih kunci Apakah Anda percaya kunci-kunci ini? Apakah Kamu cukup percaya untuk tetap menggunakan kunci-kunci ini? Enkripsi Papan Klip dengan _Frasa Sandi Keluar Sidik jari: Kepercayaan Penuh Galat GnuPG Hasil GnuPG Sembunyikan penerima Sembunyikan ID pengguna penerima pesan yang dienkripsi. Jika tidak, semua orang yang dapat melihat pesan terenkripsi dapat melihat siapa saja penerimanya. ID Kunci Kepercayaan Marginal Nama Tidak ada kunci tersedia Tidak ada kunci terpilih Tidak ada (Jangan tandai) Applet enkripsi OpenPGP Pesan-pesan lain dari GnuPG: Keluaran GnuPG: Pilih penerima: Tandai pesan sebagai: Tandai/Enkripsi Papan Klip dengan _Kunci Publik Status Papan klip ini tidak berisi data masukan yang valid. Kunci terpilih berikut tidak sepenuhnya bisa dipercaya: Karenanya pengoperasian tidak dapat dilaksanakan. Kepercayaan Tertinggi Kepercayaan Tidak Dikenal ID Pengguna: Kamu akan meninggalkan applet OpenPGP. Apa kamu yakin? Anda harus memiliki private key untuk menandai pesan, atau beberapa public key untuk mengenkripsi pesan, atau keduanya. Anda memerlukan private key untuk menandai pesan atau public key untuk mengenkripsi pesan. _Dekrip/Verifikasi Papan Klip _Kelola Kunci _Buka Penyunting Teks 