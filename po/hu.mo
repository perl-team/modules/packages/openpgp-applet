��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  �       �     �     �   �  �   #  	�   	  	�     	�     	�   
  	�     	�     	�   �  

     
�     
�     
�     
�     
�              $  ;     `     o     �   8  �     �   4  �   ~     '  �     �     �   %  �   C      �  d   n  �   (  m     �     � About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Robert Zsolt <physx_ms@outlook.com>
Language-Team: Hungarian (http://www.transifex.com/otf/torproject/language/hu/)
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Névjegy Válasszon kulcsokat Megbízik ebben a kulcsban? Eléggé megbízik ebben a kulcsban, hogy mindenképpen használja? Eléggé megbízik ezekben a kulcsokban, hogy mindenképpen használja őket? Vágólap titkosítása _Jelszóval Kilépés Ujjlenyomat: Teljes megbizhatóság GnuPG hiba GnuPG eredmények Címzettek elrejtése A titkosított üzenet összes címzettje felhasználói ID-jének  elrejtése. Egyébként aki látja a titkosított üzenetet, megtudhatja kik voltak még a címzettek. Kulcs ID Kis megbízhatóság Név Nem érhetőek el kulcsok Nincs kulcs kiválasztva Nincs (Nem ír alá) OpenPGP titkosító applet Egyéb GnuPG által adott üzenetek: GnuPG kimenet: Címzettek kiválasztása: Üzenet aláírása mint: Vágólap aláírása/titkosítása Publikus _kulcsokkal Állapot A vágólap nem tartalmaz érvényes bemeneti adatot A következő, kiválasztott kulcs nem teljesen megbízható: A következő, kiválasztott kulcsok nem teljesen megbízhatók: Ezért a művelet nem hajtható végre. Legmagasabb megbizhatóság Ismeretlen megbízhatóság Felhasználó ID: Felhasználó ID-k: Arra készül, hogy kilépjen az OpenPGP Applet-ből. Biztos benne? Az üzenet aláírásához ki kell választani egy privát kulcsot, vagy valamelyik nyílvános kulcsot esetleg mindkettőt az üzenet titkosításához. Szükséged van egy privát kulcsra az üzenet aláírásához vagy egy nyílvános kulcsra a titkosításhoz. Vágólap _Kititkosítása/Ellenőrzése _Kulcsok kezelése _Szövegeditor megnyitása 