��       &     L              |     }     �     �   a  �   "  
     -     2   
  ?     J     V     d   �  t          	               /     @     R   !  l     �     �     �   (  �     �   0  �   c  $   ,  �     �     �     �   3  �   g     L  �     �     �     �  �     	  �     �   #  �   �  	   4  	�     	�     	�     	�     	�     
	     
   �  
6     
�     
�     
�        &  "     I     _   #  }     �   !  �     �   ;  �     1   @  7   z  x   =  �     1     C   "  V   I  y   o  �   b  3   (  �     �     � About Choose keys Do you trust these keys? Do you trust this key enough to use it anyway? Do you trust these keys enough to use them anyway? Encrypt Clipboard with _Passphrase Exit Fingerprint: Full Trust GnuPG error GnuPG results Hide recipients Hide the user IDs of all recipients of an encrypted message. Otherwise anyone that sees the encrypted message can see who the recipients are. Key ID Marginal Trust Name No keys available No keys selected None (Don't sign) OpenPGP encryption applet Other messages provided by GnuPG: Output of GnuPG: Select recipients: Sign message as: Sign/Encrypt Clipboard with Public _Keys Status The clipboard does not contain valid input data. The following selected key is not fully trusted: The following selected keys are not fully trusted: Therefore the operation cannot be performed. Ultimate Trust Unknown Trust User ID: User IDs: You are about to exit OpenPGP Applet. Are you sure? You must select a private key to sign the message, or some public keys to encrypt the message, or both. You need a private key to sign messages or a public key to encrypt messages. _Decrypt/Verify Clipboard _Manage Keys _Open Text Editor Project-Id-Version: The Tor Project
Report-Msgid-Bugs-To: tails@boum.org
POT-Creation-Date: 2017-08-05 15:07-0400
PO-Revision-Date: 2018-02-20 19:11+0000
Last-Translator: Donncha Ó Cearbhaill <donncha@equalit.ie>
Language-Team: French (Canada) (http://www.transifex.com/otf/torproject/language/fr_CA/)
Language: fr_CA
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 À propos Choisir les clés Faites-vous confiance à ces clés? Faites-vous quand même assez confiance à cette clé pour l’utiliser? Faites-vous quand même assez confiance à ces clés pour les utiliser? Chiffrer le presse-papiers avec une _phrase de passe Quitter Empreinte : Confiance complète Erreur de GnuPG Résultats de GnuPG Cacher les destinataires Cacher les ID utilisateur de tous les destinataires d’un message chiffré. Autrement, quiconque voit le message chiffré peut aussi voir qui sont les destinataires. ID de la clé Confiance marginale Nom Aucune clé n’est disponible Aucune clé n’a été sélectionnée Aucun (ne pas signer) Applet de chiffrement OpenPGP Autres messages fournis par GnuPG : Sortie de GnuPG : Sélectionner les destinataires : Signer le message en tant que : Signer/chiffrer le presse-papiers avec des _clés publiques État Le presse-papiers ne contient aucune donnée d’entrée valide. La clé choisie suivante n’est pas entièrement fiable : Les clés choisies suivantes ne sont pas entièrement fiables : Par conséquent, l’opération ne peut pas être effectuée. Confiance absolue Confiance inconnue ID utilisateur : ID utilisateur :  Vous êtes sur le point de quitter l’applet OpnPGP. Êtes-vous certain? Vous devez choisir une clé privée pour signer le message, ou une clé publique pour le chiffrer, ou les deux. Il vous faut une une clé privée pour signer les messages ou une clé publique pour les chiffrer. _Déchiffrer/vérifier le presse-papiers _Gérer les clés _Ouvrir l’éditeur de texte 