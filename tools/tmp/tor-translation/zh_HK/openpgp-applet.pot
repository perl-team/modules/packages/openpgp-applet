# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Tails developers
# This file is distributed under the same license as the OpenPGP_Applet package.
# 
# Translators:
# 大圈洋蔥, 2016
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: tails@boum.org\n"
"POT-Creation-Date: 2017-08-05 15:07-0400\n"
"PO-Revision-Date: 2018-02-20 19:11+0000\n"
"Last-Translator: Chi-Hsun Tsai\n"
"Language-Team: Chinese (Hong Kong) (http://www.transifex.com/otf/torproject/language/zh_HK/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: zh_HK\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: bin/openpgp-applet:160
msgid "You are about to exit OpenPGP Applet. Are you sure?"
msgstr "離開OpenPGP加密程式。確定？"

#: bin/openpgp-applet:172
msgid "OpenPGP encryption applet"
msgstr "OpenPGP加密小程式"

#: bin/openpgp-applet:175
msgid "Exit"
msgstr "離開"

#: bin/openpgp-applet:177
msgid "About"
msgstr "關於"

#: bin/openpgp-applet:232
msgid "Encrypt Clipboard with _Passphrase"
msgstr "使用_Passphrase對剪貼簿進行加密"

#: bin/openpgp-applet:235
msgid "Sign/Encrypt Clipboard with Public _Keys"
msgstr "使用公匙簽署╱加密剪貼簿"

#: bin/openpgp-applet:240
msgid "_Decrypt/Verify Clipboard"
msgstr "_Decrypt╱驗證剪貼簿"

#: bin/openpgp-applet:244
msgid "_Manage Keys"
msgstr "_Manage鎖匙"

#: bin/openpgp-applet:248
msgid "_Open Text Editor"
msgstr "_Open Text Editor"

#: bin/openpgp-applet:292
msgid "The clipboard does not contain valid input data."
msgstr "剪貼簿未包含有效嘅輸入資料。"

#: bin/openpgp-applet:337 bin/openpgp-applet:339 bin/openpgp-applet:341
msgid "Unknown Trust"
msgstr "不明嘅信任"

#: bin/openpgp-applet:343
msgid "Marginal Trust"
msgstr "臨界信任"

#: bin/openpgp-applet:345
msgid "Full Trust"
msgstr "完全信任"

#: bin/openpgp-applet:347
msgid "Ultimate Trust"
msgstr "終極信任"

#: bin/openpgp-applet:400
msgid "Name"
msgstr "名稱"

#: bin/openpgp-applet:401
msgid "Key ID"
msgstr "鎖匙ID"

#: bin/openpgp-applet:402
msgid "Status"
msgstr "狀態"

#: bin/openpgp-applet:433
msgid "Fingerprint:"
msgstr "數碼指紋："

#: bin/openpgp-applet:436
msgid "User ID:"
msgid_plural "User IDs:"
msgstr[0] "用戶識別碼："

#: bin/openpgp-applet:465
msgid "None (Don't sign)"
msgstr "無(未簽署)"

#: bin/openpgp-applet:528
msgid "Select recipients:"
msgstr "選取收件者："

#: bin/openpgp-applet:536
msgid "Hide recipients"
msgstr "隱藏收件者"

#: bin/openpgp-applet:539
msgid ""
"Hide the user IDs of all recipients of an encrypted message. Otherwise "
"anyone that sees the encrypted message can see who the recipients are."
msgstr "隱藏加密郵件全部收件者嘅用戶識別碼。否則任何人見到加密嘅郵件都可以睇出邊個係收件者。"

#: bin/openpgp-applet:545
msgid "Sign message as:"
msgstr "郵件簽名為："

#: bin/openpgp-applet:549
msgid "Choose keys"
msgstr "選取鎖匙"

#: bin/openpgp-applet:589
msgid "Do you trust these keys?"
msgstr "信任呢啲鎖匙嗎？"

#: bin/openpgp-applet:592
msgid "The following selected key is not fully trusted:"
msgid_plural "The following selected keys are not fully trusted:"
msgstr[0] "下列所選嘅鎖匙未被完全信任："

#: bin/openpgp-applet:610
msgid "Do you trust this key enough to use it anyway?"
msgid_plural "Do you trust these keys enough to use them anyway?"
msgstr[0] "可以信任呢啲鎖匙到足以使用佢哋嗎？"

#: bin/openpgp-applet:623
msgid "No keys selected"
msgstr "未選取鎖匙"

#: bin/openpgp-applet:625
msgid ""
"You must select a private key to sign the message, or some public keys to "
"encrypt the message, or both."
msgstr "須選取私人鎖匙對郵件進行簽名，或一啲公匙嚟加密郵件，或兩者兼行。"

#: bin/openpgp-applet:653
msgid "No keys available"
msgstr "無可用鎖匙"

#: bin/openpgp-applet:655
msgid ""
"You need a private key to sign messages or a public key to encrypt messages."
msgstr "須選取私人鎖匙對郵件進行簽名，或公匙嚟加密郵件。"

#: bin/openpgp-applet:783
msgid "GnuPG error"
msgstr "GnuPG錯誤"

#: bin/openpgp-applet:804
msgid "Therefore the operation cannot be performed."
msgstr "因此唔能夠執行操作。"

#: bin/openpgp-applet:854
msgid "GnuPG results"
msgstr "GnuPG結果"

#: bin/openpgp-applet:860
msgid "Output of GnuPG:"
msgstr "GnuPG嘅輸出："

#: bin/openpgp-applet:885
msgid "Other messages provided by GnuPG:"
msgstr "由GnuPG提供嘅其他訊息："
