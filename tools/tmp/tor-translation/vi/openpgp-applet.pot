# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Tails developers
# This file is distributed under the same license as the OpenPGP_Applet package.
# 
# Translators:
# Acooldude, 2016
# Khanh Nguyen <nguyenduykhanh85@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: tails@boum.org\n"
"POT-Creation-Date: 2017-08-05 15:07-0400\n"
"PO-Revision-Date: 2018-02-20 19:11+0000\n"
"Last-Translator: Khanh Nguyen <nguyenduykhanh85@gmail.com>\n"
"Language-Team: Vietnamese (http://www.transifex.com/otf/torproject/language/vi/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: vi\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: bin/openpgp-applet:160
msgid "You are about to exit OpenPGP Applet. Are you sure?"
msgstr "Bạn định thoát khỏi OpenPGP Applet. Bạn chắc chứ?"

#: bin/openpgp-applet:172
msgid "OpenPGP encryption applet"
msgstr "Ứng dụng mã hóa OpenPGP"

#: bin/openpgp-applet:175
msgid "Exit"
msgstr "Thoát"

#: bin/openpgp-applet:177
msgid "About"
msgstr "Về"

#: bin/openpgp-applet:232
msgid "Encrypt Clipboard with _Passphrase"
msgstr "Mã hóa Clipboard với _Passphrase"

#: bin/openpgp-applet:235
msgid "Sign/Encrypt Clipboard with Public _Keys"
msgstr "Ký/Mã hóa clipboard với Public _Keys"

#: bin/openpgp-applet:240
msgid "_Decrypt/Verify Clipboard"
msgstr "_Decrypt/Verify Clipboard"

#: bin/openpgp-applet:244
msgid "_Manage Keys"
msgstr "_Manage Keys"

#: bin/openpgp-applet:248
msgid "_Open Text Editor"
msgstr "_Open Text Editor"

#: bin/openpgp-applet:292
msgid "The clipboard does not contain valid input data."
msgstr "Clipboard không chứa dữ liệu nhập vào hợp lệ."

#: bin/openpgp-applet:337 bin/openpgp-applet:339 bin/openpgp-applet:341
msgid "Unknown Trust"
msgstr "Sự tin tưởng không được biết đến"

#: bin/openpgp-applet:343
msgid "Marginal Trust"
msgstr "Sự tin cậy hạn chế"

#: bin/openpgp-applet:345
msgid "Full Trust"
msgstr "Sự tin cậy đầy đủ"

#: bin/openpgp-applet:347
msgid "Ultimate Trust"
msgstr "Sự tin cậy tối đa"

#: bin/openpgp-applet:400
msgid "Name"
msgstr "Tên"

#: bin/openpgp-applet:401
msgid "Key ID"
msgstr "ID của khóa"

#: bin/openpgp-applet:402
msgid "Status"
msgstr "Tình trạng"

#: bin/openpgp-applet:433
msgid "Fingerprint:"
msgstr "Dấu vân tay:"

#: bin/openpgp-applet:436
msgid "User ID:"
msgid_plural "User IDs:"
msgstr[0] "IDs của những người dùng:"

#: bin/openpgp-applet:465
msgid "None (Don't sign)"
msgstr "Không có gì (Đừng ký)"

#: bin/openpgp-applet:528
msgid "Select recipients:"
msgstr "Lựa chọn những người nhận:"

#: bin/openpgp-applet:536
msgid "Hide recipients"
msgstr "Ẩn những người nhận"

#: bin/openpgp-applet:539
msgid ""
"Hide the user IDs of all recipients of an encrypted message. Otherwise "
"anyone that sees the encrypted message can see who the recipients are."
msgstr "Ẩn những ID của người dùng của tất cả những người nhận một thông điệp được mã hóa."

#: bin/openpgp-applet:545
msgid "Sign message as:"
msgstr "Ký thông điệp như là:"

#: bin/openpgp-applet:549
msgid "Choose keys"
msgstr "Chọn những khóa"

#: bin/openpgp-applet:589
msgid "Do you trust these keys?"
msgstr "Bạn có tin cậy những khóa này?"

#: bin/openpgp-applet:592
msgid "The following selected key is not fully trusted:"
msgid_plural "The following selected keys are not fully trusted:"
msgstr[0] "Những khóa được lữa chọn dưới đây không được tin cậy tuyệt đối:"

#: bin/openpgp-applet:610
msgid "Do you trust this key enough to use it anyway?"
msgid_plural "Do you trust these keys enough to use them anyway?"
msgstr[0] "Bạn có tin cậy những khóa này đủ để sử dụng chúng dù thế nào đi nữa?"

#: bin/openpgp-applet:623
msgid "No keys selected"
msgstr "Không có khóa nào được lựa chọn"

#: bin/openpgp-applet:625
msgid ""
"You must select a private key to sign the message, or some public keys to "
"encrypt the message, or both."
msgstr "Bạn phải lựa chọn khóa cá nhân để ký thông điệp, hoặc một vài khóa công khai để mã hóa thông điệp, hoặc cả hai."

#: bin/openpgp-applet:653
msgid "No keys available"
msgstr "Không có sẵn khóa nào"

#: bin/openpgp-applet:655
msgid ""
"You need a private key to sign messages or a public key to encrypt messages."
msgstr "Bạn cần một khóa cá nhân để ký thông điệp hoặc một khóa công khai để mã hóa thông điệp."

#: bin/openpgp-applet:783
msgid "GnuPG error"
msgstr "Lỗi GnuPG"

#: bin/openpgp-applet:804
msgid "Therefore the operation cannot be performed."
msgstr "Cho nên việc vận hành không thể thực hiện."

#: bin/openpgp-applet:854
msgid "GnuPG results"
msgstr "Những kết quả GnuPG"

#: bin/openpgp-applet:860
msgid "Output of GnuPG:"
msgstr "Đầu ra của GnuPG"

#: bin/openpgp-applet:885
msgid "Other messages provided by GnuPG:"
msgstr "Những thông điệp khác được cung cấp bởi GnuPG"
