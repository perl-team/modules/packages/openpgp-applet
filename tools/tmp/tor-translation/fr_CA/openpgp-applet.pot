# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Tails developers
# This file is distributed under the same license as the OpenPGP_Applet package.
# 
# Translators:
# French language coordinator <french.coordinator@rbox.me>, 2016
# French language coordinator <french.coordinator@rbox.me>, 2015
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: tails@boum.org\n"
"POT-Creation-Date: 2017-08-05 15:07-0400\n"
"PO-Revision-Date: 2018-02-20 19:11+0000\n"
"Last-Translator: Donncha Ó Cearbhaill <donncha@equalit.ie>\n"
"Language-Team: French (Canada) (http://www.transifex.com/otf/torproject/language/fr_CA/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr_CA\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: bin/openpgp-applet:160
msgid "You are about to exit OpenPGP Applet. Are you sure?"
msgstr "Vous êtes sur le point de quitter l’applet OpnPGP. Êtes-vous certain?"

#: bin/openpgp-applet:172
msgid "OpenPGP encryption applet"
msgstr "Applet de chiffrement OpenPGP"

#: bin/openpgp-applet:175
msgid "Exit"
msgstr "Quitter"

#: bin/openpgp-applet:177
msgid "About"
msgstr "À propos"

#: bin/openpgp-applet:232
msgid "Encrypt Clipboard with _Passphrase"
msgstr "Chiffrer le presse-papiers avec une _phrase de passe"

#: bin/openpgp-applet:235
msgid "Sign/Encrypt Clipboard with Public _Keys"
msgstr "Signer/chiffrer le presse-papiers avec des _clés publiques"

#: bin/openpgp-applet:240
msgid "_Decrypt/Verify Clipboard"
msgstr "_Déchiffrer/vérifier le presse-papiers"

#: bin/openpgp-applet:244
msgid "_Manage Keys"
msgstr "_Gérer les clés"

#: bin/openpgp-applet:248
msgid "_Open Text Editor"
msgstr "_Ouvrir l’éditeur de texte"

#: bin/openpgp-applet:292
msgid "The clipboard does not contain valid input data."
msgstr "Le presse-papiers ne contient aucune donnée d’entrée valide."

#: bin/openpgp-applet:337 bin/openpgp-applet:339 bin/openpgp-applet:341
msgid "Unknown Trust"
msgstr "Confiance inconnue"

#: bin/openpgp-applet:343
msgid "Marginal Trust"
msgstr "Confiance marginale"

#: bin/openpgp-applet:345
msgid "Full Trust"
msgstr "Confiance complète"

#: bin/openpgp-applet:347
msgid "Ultimate Trust"
msgstr "Confiance absolue"

#: bin/openpgp-applet:400
msgid "Name"
msgstr "Nom"

#: bin/openpgp-applet:401
msgid "Key ID"
msgstr "ID de la clé"

#: bin/openpgp-applet:402
msgid "Status"
msgstr "État"

#: bin/openpgp-applet:433
msgid "Fingerprint:"
msgstr "Empreinte :"

#: bin/openpgp-applet:436
msgid "User ID:"
msgid_plural "User IDs:"
msgstr[0] "ID utilisateur :"
msgstr[1] "ID utilisateur : "

#: bin/openpgp-applet:465
msgid "None (Don't sign)"
msgstr "Aucun (ne pas signer)"

#: bin/openpgp-applet:528
msgid "Select recipients:"
msgstr "Sélectionner les destinataires :"

#: bin/openpgp-applet:536
msgid "Hide recipients"
msgstr "Cacher les destinataires"

#: bin/openpgp-applet:539
msgid ""
"Hide the user IDs of all recipients of an encrypted message. Otherwise "
"anyone that sees the encrypted message can see who the recipients are."
msgstr "Cacher les ID utilisateur de tous les destinataires d’un message chiffré. Autrement, quiconque voit le message chiffré peut aussi voir qui sont les destinataires."

#: bin/openpgp-applet:545
msgid "Sign message as:"
msgstr "Signer le message en tant que :"

#: bin/openpgp-applet:549
msgid "Choose keys"
msgstr "Choisir les clés"

#: bin/openpgp-applet:589
msgid "Do you trust these keys?"
msgstr "Faites-vous confiance à ces clés?"

#: bin/openpgp-applet:592
msgid "The following selected key is not fully trusted:"
msgid_plural "The following selected keys are not fully trusted:"
msgstr[0] "La clé choisie suivante n’est pas entièrement fiable :"
msgstr[1] "Les clés choisies suivantes ne sont pas entièrement fiables :"

#: bin/openpgp-applet:610
msgid "Do you trust this key enough to use it anyway?"
msgid_plural "Do you trust these keys enough to use them anyway?"
msgstr[0] "Faites-vous quand même assez confiance à cette clé pour l’utiliser?"
msgstr[1] "Faites-vous quand même assez confiance à ces clés pour les utiliser?"

#: bin/openpgp-applet:623
msgid "No keys selected"
msgstr "Aucune clé n’a été sélectionnée"

#: bin/openpgp-applet:625
msgid ""
"You must select a private key to sign the message, or some public keys to "
"encrypt the message, or both."
msgstr "Vous devez choisir une clé privée pour signer le message, ou une clé publique pour le chiffrer, ou les deux."

#: bin/openpgp-applet:653
msgid "No keys available"
msgstr "Aucune clé n’est disponible"

#: bin/openpgp-applet:655
msgid ""
"You need a private key to sign messages or a public key to encrypt messages."
msgstr "Il vous faut une une clé privée pour signer les messages ou une clé publique pour les chiffrer."

#: bin/openpgp-applet:783
msgid "GnuPG error"
msgstr "Erreur de GnuPG"

#: bin/openpgp-applet:804
msgid "Therefore the operation cannot be performed."
msgstr "Par conséquent, l’opération ne peut pas être effectuée."

#: bin/openpgp-applet:854
msgid "GnuPG results"
msgstr "Résultats de GnuPG"

#: bin/openpgp-applet:860
msgid "Output of GnuPG:"
msgstr "Sortie de GnuPG :"

#: bin/openpgp-applet:885
msgid "Other messages provided by GnuPG:"
msgstr "Autres messages fournis par GnuPG :"
